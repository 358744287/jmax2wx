package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WeixinResponse;
import com.xdtech.wx.vo.WeixinResponseItem;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see
 */
public interface WeixinResponseService extends IBaseService<WeixinResponse>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWeixinResponse(WeixinResponseItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param newId
	 * @return
	 */
	WeixinResponseItem loadWeixinResponseItem(Long weixinResponseId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWeixinResponseInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param weixinResponseIds
	 */
	boolean deleteWeixinResponseInfo(List<Long> weixinResponseIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @return
	 */
	List<WeixinResponseItem> loadItems();

	/**
	 * 根据微信帐号，获取回复列表
	 * @author <a href="max.zheng@zkteco.com">郑志雄</>
	 * @since 2016-3-11 下午3:47:07
	 * @param accountId
	 * @return
	 */
	List<WeixinResponse> getResponseByAccountId(String accountId);
}
