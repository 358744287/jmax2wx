package com.xdtech.wx.service;

import java.util.List;
import java.util.Map;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxZklyTeam;
import com.xdtech.wx.vo.WxZklyTeamItem;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see
 */
public interface WxZklyTeamService extends IBaseService<WxZklyTeam>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxZklyTeam(WxZklyTeamItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxZklyTeamItem loadWxZklyTeamItem(Long wxZklyTeamId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxZklyTeamInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param wxZklyTeamIds
	 */
	boolean deleteWxZklyTeamInfo(List<Long> wxZklyTeamIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @return
	 */
	List<WxZklyTeamItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @return
	 */
	public List<WxZklyTeamItem> getWxZklyTeamByCondition(
			WxZklyTeamItem condition);

	Map<String, Object> loadTongJi();
}
