package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WeixinAccount;
import com.xdtech.wx.vo.WeixinAccountItem;

/**
 * 
 * @author max.zheng
 * @create 2016-03-10 22:36:32
 * @since 1.0
 * @see
 */
public interface WeixinAccountService extends IBaseService<WeixinAccount>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWeixinAccount(WeixinAccountItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param newId
	 * @return
	 */
	WeixinAccountItem loadWeixinAccountItem(Long weixinAccountId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWeixinAccountInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param weixinAccountIds
	 */
	boolean deleteWeixinAccountInfo(List<Long> weixinAccountIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @return
	 */
	List<WeixinAccountItem> loadItems();

	/**
	 * 
	 * @author max.zheng
	 * @create 2016-3-10下午10:48:19
	 * @modified by
	 * @param toUserName
	 * @return
	 */
	WeixinAccount findByToUsername(String toUserName);

	/**
	 * 根据微信原始id获取公众账号
	 * @author max.zheng
	 * @create 2016-3-10下午10:49:21
	 * @modified by
	 * @param toUserName
	 * @return
	 */
	WeixinAccount findByAccountId(String toUserName);
}
