package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WeixinReceiveMsg;
import com.xdtech.wx.vo.WeixinReceiveMsgItem;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see
 */
public interface WeixinReceiveMsgService extends IBaseService<WeixinReceiveMsg>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWeixinReceiveMsg(WeixinReceiveMsgItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param newId
	 * @return
	 */
	WeixinReceiveMsgItem loadWeixinReceiveMsgItem(Long weixinReceiveMsgId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWeixinReceiveMsgInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param weixinReceiveMsgIds
	 */
	boolean deleteWeixinReceiveMsgInfo(List<Long> weixinReceiveMsgIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @return
	 */
	List<WeixinReceiveMsgItem> loadItems();
}
