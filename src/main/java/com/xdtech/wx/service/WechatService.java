/*
 * Project Name: jmax
 * File Name: WechatService.java
 * Copyright: Copyright(C) 1985-2016 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.wx.service;

import com.xdtech.wx.message.resp.TextMessageResp;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2016-3-11 下午2:37:57
 */
public interface WechatService
{

	/**
	 * 
	 * @author <a href="max.zheng@zkteco.com">郑志雄</>
	 * @since 2016-3-11 下午2:41:41
	 * @param fromUserName   发送者
	 * @param toUserName     公众号接收者
	 * @param createTime     消息时间
	 * @param msgType		  消息类型  text
	 * @param content        消息内容
	 * @param msgId          消息id
	 * @return
	 */
	String handleTextMsg(String fromUserName, String toUserName, String createTime, String msgType, String content, String msgId,TextMessageResp textMessage);

}
