package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxUserInfo;
import com.xdtech.wx.vo.WxUserInfoItem;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:24
 * @since 1.0
 * @see
 */
public interface WxUserInfoService extends IBaseService<WxUserInfo>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxUserInfo(WxUserInfoItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxUserInfoItem loadWxUserInfoItem(Long wxUserInfoId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxUserInfoInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param wxUserInfoIds
	 */
	boolean deleteWxUserInfoInfo(List<Long> wxUserInfoIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @return
	 */
	List<WxUserInfoItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @return
	 */
	public List<WxUserInfoItem> getWxUserInfoByCondition(
			WxUserInfoItem condition);

	WxUserInfo getWxUserInfoByOpenId(String companyCode, String openId);

	void wxUserInfoSign(WxUserInfoItem userInfo);

	int getSignPersonCount(String companyCode);

	void saveImportWxUserInfos(List<WxUserInfoItem> items);

	WxUserInfo getWxUserByName(String name);

	WxUserInfoItem getUserInfoByOpenId(String openId);

	void resetWxUserInfoInfo(List<Long> wxUserInfoIds);
}
