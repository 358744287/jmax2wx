package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxZklyMember;
import com.xdtech.wx.vo.WxZklyMemberItem;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:24
 * @since 1.0
 * @see
 */
public interface WxZklyMemberService extends IBaseService<WxZklyMember>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxZklyMember(WxZklyMemberItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxZklyMemberItem loadWxZklyMemberItem(Long wxZklyMemberId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxZklyMemberInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param wxZklyMemberIds
	 */
	boolean deleteWxZklyMemberInfo(List<Long> wxZklyMemberIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @return
	 */
	List<WxZklyMemberItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @return
	 */
	public List<WxZklyMemberItem> getWxZklyMemberByCondition(
			WxZklyMemberItem condition);
}
