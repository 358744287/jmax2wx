package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxZklyTeamDao;
import com.xdtech.wx.model.WxZklyTeam;
import com.xdtech.wx.service.WxZklyDayService;
import com.xdtech.wx.service.WxZklyMemberService;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyDayItem;
import com.xdtech.wx.vo.WxZklyMemberItem;
import com.xdtech.wx.vo.WxZklyTeamItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see
 */
@Service
public class WxZklyTeamServiceImpl implements WxZklyTeamService {
	private Log log = LogFactory.getLog(WxZklyTeamServiceImpl.class);
	@Autowired
	private WxZklyTeamDao wxZklyTeamDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private WxZklyDayService wxZklyDayService;
	@Autowired
	private WxZklyMemberService wxZklyMemberService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param entity
	 */
	public void save(WxZklyTeam entity) {
		wxZklyTeamDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxZklyTeam> entities) {
		wxZklyTeamDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param entity
	 */
	public void delete(WxZklyTeam entity) {
		wxZklyTeamDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxZklyTeamDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxZklyTeam get(Long id) {
		return wxZklyTeamDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @return
	 */
	public List<WxZklyTeam> getAll() {
		return wxZklyTeamDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxZklyTeams = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxZklyTeam> page = wxZklyTeamDao.findPage(pg,"from WxZklyTeam order by createTime desc", values);
			wxZklyTeams = BeanUtils.copyListProperties(
					WxZklyTeamItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxZklyTeam> wxZklyTeamList = wxZklyTeamDao.find("from WxZklyTeam order by id desc", values);
			wxZklyTeams = BeanUtils.copyListProperties(
					WxZklyTeamItem.class, wxZklyTeamList);
			rows = wxZklyTeams.size();
		}
		results.put("total", rows);
		results.put("rows", wxZklyTeams);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxZklyTeam(WxZklyTeamItem item) {
		WxZklyTeam wxZklyTeam = null;
		if (item.getId()==null) {
			wxZklyTeam = new WxZklyTeam();
		}else {
			wxZklyTeam = wxZklyTeamDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxZklyTeam, item);
		wxZklyTeamDao.save(wxZklyTeam);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param wxZklyTeamId
	 * @return
	 */
	public WxZklyTeamItem loadWxZklyTeamItem(Long wxZklyTeamId) {
		WxZklyTeam wxZklyTeam = wxZklyTeamDao.get(wxZklyTeamId);
		return WxZklyTeamItem.createItem(wxZklyTeam);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxZklyTeamInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxZklyTeamInfo(List<Long> wxZklyTeamIds) {
		for (Long id : wxZklyTeamIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:20
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxZklyTeamItem> loadItems() {
		List<WxZklyTeam> wxZklyTeams = getAll();
		return WxZklyTeamItem.createItems(wxZklyTeams);
	}
	
	public List<WxZklyTeamItem> getWxZklyTeamByCondition(
			WxZklyTeamItem condition) {
		List<WxZklyTeamItem> items = baseDao.findByNamedQuery("findWxZklyTeamByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public Map<String, Object> loadTongJi() {
		List<WxZklyTeamItem> items = getWxZklyTeamByCondition(new WxZklyTeamItem());
		WxZklyDayItem dayCondition = new WxZklyDayItem();
		WxZklyMemberItem memberCondition = new WxZklyMemberItem();
		for (WxZklyTeamItem wxZklyTeamItem : items) {
			dayCondition.setWxZklyTeamId(wxZklyTeamItem.getId());
			memberCondition.setWxZklyTeamId(wxZklyTeamItem.getId());
			wxZklyTeamItem.setDayItems(wxZklyDayService.getWxZklyDayByCondition(dayCondition));
			wxZklyTeamItem.setMemberItems(wxZklyMemberService.getWxZklyMemberByCondition(memberCondition));
		}

		Map<String, Object> mapItems = new HashMap<String, Object>();
		mapItems.put("all", items);
		for (WxZklyTeamItem wxZklyTeamItem : items) {
			mapItems.put(wxZklyTeamItem.getId()+"", wxZklyTeamItem);
		}
		return mapItems;
	}

}
