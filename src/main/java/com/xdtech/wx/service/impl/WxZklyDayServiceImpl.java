package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxZklyDayDao;
import com.xdtech.wx.model.WxZklyDay;
import com.xdtech.wx.service.WxZklyDayService;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyDayItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see
 */
@Service
public class WxZklyDayServiceImpl implements WxZklyDayService {
	private Log log = LogFactory.getLog(WxZklyDayServiceImpl.class);
	@Autowired
	private WxZklyDayDao wxZklyDayDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private WxZklyTeamService wxZklyTeamService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param entity
	 */
	public void save(WxZklyDay entity) {
		wxZklyDayDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxZklyDay> entities) {
		wxZklyDayDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param entity
	 */
	public void delete(WxZklyDay entity) {
		wxZklyDayDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxZklyDayDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxZklyDay get(Long id) {
		return wxZklyDayDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @return
	 */
	public List<WxZklyDay> getAll() {
		return wxZklyDayDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxZklyDays = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxZklyDay> page = wxZklyDayDao.findPage(pg,"from WxZklyDay order by createTime desc", values);
			wxZklyDays = BeanUtils.copyListProperties(
					WxZklyDayItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxZklyDay> wxZklyDayList = wxZklyDayDao.find("from WxZklyDay order by id desc", values);
			wxZklyDays = BeanUtils.copyListProperties(
					WxZklyDayItem.class, wxZklyDayList);
			rows = wxZklyDays.size();
		}
		results.put("total", rows);
		results.put("rows", wxZklyDays);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxZklyDay(WxZklyDayItem item) {
		WxZklyDay wxZklyDay = null;
		if (item.getId()==null) {
			wxZklyDay = new WxZklyDay();
			wxZklyDay.setWxZklyTeam(wxZklyTeamService.get(item.getWxZklyTeamId()));
		}else {
			wxZklyDay = wxZklyDayDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxZklyDay, item);
		wxZklyDayDao.save(wxZklyDay);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param wxZklyDayId
	 * @return
	 */
	public WxZklyDayItem loadWxZklyDayItem(Long wxZklyDayId) {
		WxZklyDay wxZklyDay = wxZklyDayDao.get(wxZklyDayId);
		return WxZklyDayItem.createItem(wxZklyDay);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxZklyDayInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxZklyDayInfo(List<Long> wxZklyDayIds) {
		for (Long id : wxZklyDayIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxZklyDayItem> loadItems() {
		List<WxZklyDay> wxZklyDays = getAll();
		return WxZklyDayItem.createItems(wxZklyDays);
	}
	
	public List<WxZklyDayItem> getWxZklyDayByCondition(
			WxZklyDayItem condition) {
		List<WxZklyDayItem> items = baseDao.findByNamedQuery("findWxZklyDayByCondition",condition,condition.loadQueryFields());
		return items;
	}

}
