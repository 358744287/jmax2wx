package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WeixinAccountDao;
import com.xdtech.wx.model.WeixinAccount;
import com.xdtech.wx.service.WeixinAccountService;
import com.xdtech.wx.vo.WeixinAccountItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2016-03-10 22:36:32
 * @since 1.0
 * @see
 */
@Service
public class WeixinAccountServiceImpl implements WeixinAccountService {
	private Log log = LogFactory.getLog(WeixinAccountServiceImpl.class);
	@Autowired
	private WeixinAccountDao weixinAccountDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param entity
	 */
	public void save(WeixinAccount entity) {
		weixinAccountDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WeixinAccount> entities) {
		weixinAccountDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param entity
	 */
	public void delete(WeixinAccount entity) {
		weixinAccountDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		weixinAccountDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param id
	 * @return
	 */
	public WeixinAccount get(Long id) {
		return weixinAccountDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @return
	 */
	public List<WeixinAccount> getAll() {
		return weixinAccountDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> weixinAccounts = null;
		long rows = 0;
		if (pg!=null) {
			Page<WeixinAccount> page = weixinAccountDao.findPage(pg,"from WeixinAccount order by createTime desc", values);
			weixinAccounts = BeanUtils.copyListProperties(
					WeixinAccountItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WeixinAccount> weixinAccountList = weixinAccountDao.find("from WeixinAccount order by id desc", values);
			weixinAccounts = BeanUtils.copyListProperties(
					WeixinAccountItem.class, weixinAccountList);
			rows = weixinAccounts.size();
		}
		results.put("total", rows);
		results.put("rows", weixinAccounts);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWeixinAccount(WeixinAccountItem item) {
		WeixinAccount weixinAccount = null;
		if (item.getId()==null) {
			weixinAccount = new WeixinAccount();
		}else {
			weixinAccount = weixinAccountDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(weixinAccount, item);
		weixinAccountDao.save(weixinAccount);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param weixinAccountId
	 * @return
	 */
	public WeixinAccountItem loadWeixinAccountItem(Long weixinAccountId) {
		WeixinAccount weixinAccount = weixinAccountDao.get(weixinAccountId);
		return WeixinAccountItem.createItem(weixinAccount);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWeixinAccountInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWeixinAccountInfo(List<Long> weixinAccountIds) {
		for (Long id : weixinAccountIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-10 22:36:32
	 * @modified by
	 * @return
	 */
	@Override
	public List<WeixinAccountItem> loadItems() {
		List<WeixinAccount> weixinAccounts = getAll();
		return WeixinAccountItem.createItems(weixinAccounts);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-3-10下午10:49:48
	 * @modified by
	 * @param toUserName
	 * @return
	 */
	@Override
	public WeixinAccount findByToUsername(String toUserName) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-3-10下午10:49:48
	 * @modified by
	 * @param toUserName
	 * @return
	 */
	@Override
	public WeixinAccount findByAccountId(String toUserName) {
		return weixinAccountDao.findUniqueBy("originalId", toUserName);
	}

}
