package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxZklyMemberDao;
import com.xdtech.wx.model.WxZklyMember;
import com.xdtech.wx.service.WxZklyMemberService;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyMemberItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:24
 * @since 1.0
 * @see
 */
@Service
public class WxZklyMemberServiceImpl implements WxZklyMemberService {
	private Log log = LogFactory.getLog(WxZklyMemberServiceImpl.class);
	@Autowired
	private WxZklyMemberDao wxZklyMemberDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private WxZklyTeamService wxZklyTeamService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param entity
	 */
	public void save(WxZklyMember entity) {
		wxZklyMemberDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxZklyMember> entities) {
		wxZklyMemberDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param entity
	 */
	public void delete(WxZklyMember entity) {
		wxZklyMemberDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxZklyMemberDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxZklyMember get(Long id) {
		return wxZklyMemberDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @return
	 */
	public List<WxZklyMember> getAll() {
		return wxZklyMemberDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxZklyMembers = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxZklyMember> page = wxZklyMemberDao.findPage(pg,"from WxZklyMember order by createTime desc", values);
			wxZklyMembers = BeanUtils.copyListProperties(
					WxZklyMemberItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxZklyMember> wxZklyMemberList = wxZklyMemberDao.find("from WxZklyMember order by id desc", values);
			wxZklyMembers = BeanUtils.copyListProperties(
					WxZklyMemberItem.class, wxZklyMemberList);
			rows = wxZklyMembers.size();
		}
		results.put("total", rows);
		results.put("rows", wxZklyMembers);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxZklyMember(WxZklyMemberItem item) {
		WxZklyMember wxZklyMember = null;
		if (item.getId()==null) {
			wxZklyMember = new WxZklyMember();
			wxZklyMember.setWxZklyTeam(wxZklyTeamService.get(item.getWxZklyTeamId()));
		}else {
			wxZklyMember = wxZklyMemberDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxZklyMember, item);
		wxZklyMemberDao.save(wxZklyMember);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param wxZklyMemberId
	 * @return
	 */
	public WxZklyMemberItem loadWxZklyMemberItem(Long wxZklyMemberId) {
		WxZklyMember wxZklyMember = wxZklyMemberDao.get(wxZklyMemberId);
		return WxZklyMemberItem.createItem(wxZklyMember);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxZklyMemberInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxZklyMemberInfo(List<Long> wxZklyMemberIds) {
		for (Long id : wxZklyMemberIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-22 00:26:24
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxZklyMemberItem> loadItems() {
		List<WxZklyMember> wxZklyMembers = getAll();
		return WxZklyMemberItem.createItems(wxZklyMembers);
	}
	
	public List<WxZklyMemberItem> getWxZklyMemberByCondition(
			WxZklyMemberItem condition) {
		List<WxZklyMemberItem> items = baseDao.findByNamedQuery("findWxZklyMemberByCondition",condition,condition.loadQueryFields());
		return items;
	}

}
