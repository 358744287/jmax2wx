package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxPzswQuestionDao;
import com.xdtech.wx.model.WxPzswQuestion;
import com.xdtech.wx.service.WxPzswQuestionService;
import com.xdtech.wx.service.WxPzswSolveService;
import com.xdtech.wx.vo.PzswVo;
import com.xdtech.wx.vo.WxPzswQuestionItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see
 */
@Service
public class WxPzswQuestionServiceImpl implements WxPzswQuestionService {
	private Log log = LogFactory.getLog(WxPzswQuestionServiceImpl.class);
	@Autowired
	private WxPzswQuestionDao wxPzswQuestionDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private WxPzswSolveService pzswSolveService;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param entity
	 */
	public void save(WxPzswQuestion entity) {
		wxPzswQuestionDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxPzswQuestion> entities) {
		wxPzswQuestionDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param entity
	 */
	public void delete(WxPzswQuestion entity) {
		wxPzswQuestionDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxPzswQuestionDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxPzswQuestion get(Long id) {
		return wxPzswQuestionDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @return
	 */
	public List<WxPzswQuestion> getAll() {
		return wxPzswQuestionDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxPzswQuestions = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxPzswQuestion> page = wxPzswQuestionDao.findPage(pg,"from WxPzswQuestion order by createTime desc", values);
			wxPzswQuestions = BeanUtils.copyListProperties(
					WxPzswQuestionItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxPzswQuestion> wxPzswQuestionList = wxPzswQuestionDao.find("from WxPzswQuestion order by id desc", values);
			wxPzswQuestions = BeanUtils.copyListProperties(
					WxPzswQuestionItem.class, wxPzswQuestionList);
			rows = wxPzswQuestions.size();
		}
		results.put("total", rows);
		results.put("rows", wxPzswQuestions);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxPzswQuestion(WxPzswQuestionItem item) {
		WxPzswQuestion wxPzswQuestion = null;
		if (item.getId()==null) {
			wxPzswQuestion = new WxPzswQuestion();
		}else {
			wxPzswQuestion = wxPzswQuestionDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxPzswQuestion, item);
		wxPzswQuestionDao.save(wxPzswQuestion);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param wxPzswQuestionId
	 * @return
	 */
	public WxPzswQuestionItem loadWxPzswQuestionItem(Long wxPzswQuestionId) {
		WxPzswQuestion wxPzswQuestion = wxPzswQuestionDao.get(wxPzswQuestionId);
		return WxPzswQuestionItem.createItem(wxPzswQuestion);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxPzswQuestionInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxPzswQuestionInfo(List<Long> wxPzswQuestionIds) {
		for (Long id : wxPzswQuestionIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxPzswQuestionItem> loadItems() {
		List<WxPzswQuestion> wxPzswQuestions = getAll();
		return WxPzswQuestionItem.createItems(wxPzswQuestions);
	}
	
	public List<WxPzswQuestionItem> getWxPzswQuestionByCondition(
			WxPzswQuestionItem condition) {
		List<WxPzswQuestionItem> items = baseDao.findByNamedQuery("findWxPzswQuestionByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public Long saveSubmitInfo(PzswVo pzswVo) {
		WxPzswQuestion pzswQuestion = null;
		if (pzswVo.getId()!=null) {
			pzswQuestion = get(pzswVo.getId());
		}
		if (pzswQuestion==null) {
			pzswQuestion = new WxPzswQuestion();
		}
		
		pzswQuestion.setCreateTime(new Date());
		pzswQuestion.setDept(pzswVo.getDept());
		pzswQuestion.setName(pzswVo.getName());
		pzswQuestion.setModuleType(pzswVo.getModuleType());
		pzswQuestion.setQuestionName(pzswVo.getQuestionName());
		pzswQuestion.setQuestionReason1(pzswVo.getQuestionReason1());
		pzswQuestion.setQuestionReason2(pzswVo.getQuestionReason2());
		pzswQuestion.setQuestionReason3(pzswVo.getQuestionReason3());
		save(pzswQuestion);
		
		pzswSolveService.saveInfo(pzswQuestion,pzswVo);
		return pzswQuestion.getId();
	}
	@Override
	public void saveImgUrl(Long pzswId, String imgUrl) {
		if (pzswId!=null) {
			WxPzswQuestion question = get(pzswId);
			question.setUploadImg(imgUrl);
			save(question);
		}
		
	}

}
