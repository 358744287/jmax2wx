package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WeixinReceiveMsgDao;
import com.xdtech.wx.model.WeixinReceiveMsg;
import com.xdtech.wx.service.WeixinReceiveMsgService;
import com.xdtech.wx.vo.WeixinReceiveMsgItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see
 */
@Service
public class WeixinReceiveMsgServiceImpl implements WeixinReceiveMsgService {
	private Log log = LogFactory.getLog(WeixinReceiveMsgServiceImpl.class);
	@Autowired
	private WeixinReceiveMsgDao weixinReceiveMsgDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param entity
	 */
	public void save(WeixinReceiveMsg entity) {
		weixinReceiveMsgDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WeixinReceiveMsg> entities) {
		weixinReceiveMsgDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param entity
	 */
	public void delete(WeixinReceiveMsg entity) {
		weixinReceiveMsgDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		weixinReceiveMsgDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param id
	 * @return
	 */
	public WeixinReceiveMsg get(Long id) {
		return weixinReceiveMsgDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @return
	 */
	public List<WeixinReceiveMsg> getAll() {
		return weixinReceiveMsgDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> weixinReceiveMsgs = null;
		long rows = 0;
		if (pg!=null) {
			Page<WeixinReceiveMsg> page = weixinReceiveMsgDao.findPage(pg,"from WeixinReceiveMsg order by createTime desc", values);
			weixinReceiveMsgs = BeanUtils.copyListProperties(
					WeixinReceiveMsgItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WeixinReceiveMsg> weixinReceiveMsgList = weixinReceiveMsgDao.find("from WeixinReceiveMsg order by id desc", values);
			weixinReceiveMsgs = BeanUtils.copyListProperties(
					WeixinReceiveMsgItem.class, weixinReceiveMsgList);
			rows = weixinReceiveMsgs.size();
		}
		results.put("total", rows);
		results.put("rows", weixinReceiveMsgs);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWeixinReceiveMsg(WeixinReceiveMsgItem item) {
		WeixinReceiveMsg weixinReceiveMsg = null;
		if (item.getId()==null) {
			weixinReceiveMsg = new WeixinReceiveMsg();
		}else {
			weixinReceiveMsg = weixinReceiveMsgDao.get(item.getId());
		}
		//澶嶅埗鍓嶅彴淇敼鐨勫睘鎬�
		BeanUtils.copyProperties(weixinReceiveMsg, item);
		weixinReceiveMsgDao.save(weixinReceiveMsg);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param weixinReceiveMsgId
	 * @return
	 */
	public WeixinReceiveMsgItem loadWeixinReceiveMsgItem(Long weixinReceiveMsgId) {
		WeixinReceiveMsg weixinReceiveMsg = weixinReceiveMsgDao.get(weixinReceiveMsgId);
		return WeixinReceiveMsgItem.createItem(weixinReceiveMsg);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWeixinReceiveMsgInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWeixinReceiveMsgInfo(List<Long> weixinReceiveMsgIds) {
		for (Long id : weixinReceiveMsgIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:00:10
	 * @modified by
	 * @return
	 */
	@Override
	public List<WeixinReceiveMsgItem> loadItems() {
		List<WeixinReceiveMsg> weixinReceiveMsgs = getAll();
		return WeixinReceiveMsgItem.createItems(weixinReceiveMsgs);
	}

}
