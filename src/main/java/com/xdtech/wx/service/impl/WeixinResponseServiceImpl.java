package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WeixinResponseDao;
import com.xdtech.wx.model.WeixinResponse;
import com.xdtech.wx.service.WeixinResponseService;
import com.xdtech.wx.vo.WeixinResponseItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see
 */
@Service
public class WeixinResponseServiceImpl implements WeixinResponseService {
	private Log log = LogFactory.getLog(WeixinResponseServiceImpl.class);
	@Autowired
	private WeixinResponseDao weixinResponseDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param entity
	 */
	public void save(WeixinResponse entity) {
		weixinResponseDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WeixinResponse> entities) {
		weixinResponseDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param entity
	 */
	public void delete(WeixinResponse entity) {
		weixinResponseDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		weixinResponseDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param id
	 * @return
	 */
	public WeixinResponse get(Long id) {
		return weixinResponseDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @return
	 */
	public List<WeixinResponse> getAll() {
		return weixinResponseDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> weixinResponses = null;
		long rows = 0;
		if (pg!=null) {
			Page<WeixinResponse> page = weixinResponseDao.findPage(pg,"from WeixinResponse order by createTime desc", values);
			weixinResponses = BeanUtils.copyListProperties(
					WeixinResponseItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WeixinResponse> weixinResponseList = weixinResponseDao.find("from WeixinResponse order by id desc", values);
			weixinResponses = BeanUtils.copyListProperties(
					WeixinResponseItem.class, weixinResponseList);
			rows = weixinResponses.size();
		}
		results.put("total", rows);
		results.put("rows", weixinResponses);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWeixinResponse(WeixinResponseItem item) {
		WeixinResponse weixinResponse = null;
		if (item.getId()==null) {
			weixinResponse = new WeixinResponse();
		}else {
			weixinResponse = weixinResponseDao.get(item.getId());
		}
		//澶嶅埗鍓嶅彴淇敼鐨勫睘鎬�
		BeanUtils.copyProperties(weixinResponse, item);
		weixinResponseDao.save(weixinResponse);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param weixinResponseId
	 * @return
	 */
	public WeixinResponseItem loadWeixinResponseItem(Long weixinResponseId) {
		WeixinResponse weixinResponse = weixinResponseDao.get(weixinResponseId);
		return WeixinResponseItem.createItem(weixinResponse);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWeixinResponseInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWeixinResponseInfo(List<Long> weixinResponseIds) {
		for (Long id : weixinResponseIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2016-03-11 15:16:55
	 * @modified by
	 * @return
	 */
	@Override
	public List<WeixinResponseItem> loadItems() {
		List<WeixinResponse> weixinResponses = getAll();
		return WeixinResponseItem.createItems(weixinResponses);
	}
	/* (非 Javadoc)
	 * getResponseByAccountId
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param id
	 * @return
	 * @see com.xdtech.wx.service.WeixinResponseService#getResponseByAccountId(java.lang.Long)
	 */
	@Override
	public List<WeixinResponse> getResponseByAccountId(String accountId)
	{
		return weixinResponseDao.findBy("accountId", accountId);
	}

}
