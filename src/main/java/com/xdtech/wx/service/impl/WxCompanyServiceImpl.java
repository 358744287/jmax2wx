package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxCompanyDao;
import com.xdtech.wx.model.WxCompany;
import com.xdtech.wx.service.WxCompanyService;
import com.xdtech.wx.vo.WxCompanyItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see
 */
@Service
public class WxCompanyServiceImpl implements WxCompanyService {
	private Log log = LogFactory.getLog(WxCompanyServiceImpl.class);
	@Autowired
	private WxCompanyDao wxCompanyDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param entity
	 */
	public void save(WxCompany entity) {
		wxCompanyDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxCompany> entities) {
		wxCompanyDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param entity
	 */
	public void delete(WxCompany entity) {
		wxCompanyDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxCompanyDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxCompany get(Long id) {
		return wxCompanyDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @return
	 */
	public List<WxCompany> getAll() {
		return wxCompanyDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxCompanys = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxCompany> page = wxCompanyDao.findPage(pg,"from WxCompany order by createTime desc", values);
			wxCompanys = BeanUtils.copyListProperties(
					WxCompanyItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxCompany> wxCompanyList = wxCompanyDao.find("from WxCompany order by id desc", values);
			wxCompanys = BeanUtils.copyListProperties(
					WxCompanyItem.class, wxCompanyList);
			rows = wxCompanys.size();
		}
		results.put("total", rows);
		results.put("rows", wxCompanys);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxCompany(WxCompanyItem item) {
		WxCompany wxCompany = null;
		if (item.getId()==null) {
			wxCompany = new WxCompany();
		}else {
			wxCompany = wxCompanyDao.get(item.getId());
		}
		//澶嶅埗鍓嶅彴淇敼鐨勫睘鎬�
		BeanUtils.copyProperties(wxCompany, item);
		wxCompanyDao.save(wxCompany);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param wxCompanyId
	 * @return
	 */
	public WxCompanyItem loadWxCompanyItem(Long wxCompanyId) {
		WxCompany wxCompany = wxCompanyDao.get(wxCompanyId);
		return WxCompanyItem.createItem(wxCompany);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxCompanyInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxCompanyInfo(List<Long> wxCompanyIds) {
		for (Long id : wxCompanyIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxCompanyItem> loadItems() {
		List<WxCompany> wxCompanys = getAll();
		return WxCompanyItem.createItems(wxCompanys);
	}
	
	public List<WxCompanyItem> getWxCompanyByCondition(
			WxCompanyItem condition) {
		List<WxCompanyItem> items = baseDao.findByNamedQuery("findWxCompanyByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public WxCompany getCompanyByCode(String companyCode) {
		return wxCompanyDao.findUniqueBy("code", companyCode);
	}
	@Override
	public int getPersonCount(String companyCode) {
		WxCompany wxCompany = getCompanyByCode(companyCode);
		return (wxCompany==null||wxCompany.getPersonCount()==null)?0:wxCompany.getPersonCount();
	}

}
