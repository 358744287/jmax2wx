package com.xdtech.wx.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdtech.common.utils.EmptyUtil;
import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.sys.util.SysConstants;
import com.xdtech.web.model.Pagination;
import com.xdtech.wx.dao.WxUserInfoDao;
import com.xdtech.wx.model.WxCompany;
import com.xdtech.wx.model.WxUserInfo;
import com.xdtech.wx.service.WxCompanyService;
import com.xdtech.wx.service.WxUserInfoService;
import com.xdtech.wx.vo.WxUserInfoItem;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:24
 * @since 1.0
 * @see
 */
@Service
public class WxUserInfoServiceImpl implements WxUserInfoService {
	private Log log = LogFactory.getLog(WxUserInfoServiceImpl.class);
	@Autowired
	private WxUserInfoDao wxUserInfoDao;
	@Autowired
	private WxCompanyService wxCompanyService;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param entity
	 */
	public void save(WxUserInfo entity) {
		wxUserInfoDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxUserInfo> entities) {
		wxUserInfoDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param entity
	 */
	public void delete(WxUserInfo entity) {
		wxUserInfoDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxUserInfoDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxUserInfo get(Long id) {
		return wxUserInfoDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @return
	 */
	public List<WxUserInfo> getAll() {
		return wxUserInfoDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxUserInfos = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxUserInfo> page = wxUserInfoDao.findPage(pg,"from WxUserInfo order by createTime desc", values);
			wxUserInfos = BeanUtils.copyListProperties(
					WxUserInfoItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxUserInfo> wxUserInfoList = wxUserInfoDao.find("from WxUserInfo order by id desc", values);
			wxUserInfos = BeanUtils.copyListProperties(
					WxUserInfoItem.class, wxUserInfoList);
			rows = wxUserInfos.size();
		}
		results.put("total", rows);
		results.put("rows", wxUserInfos);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxUserInfo(WxUserInfoItem item) {
		WxUserInfo wxUserInfo = null;
		if (item.getId()==null) {
			wxUserInfo = new WxUserInfo();
		}else {
			wxUserInfo = wxUserInfoDao.get(item.getId());
		}
		//澶嶅埗鍓嶅彴淇敼鐨勫睘鎬�
		BeanUtils.copyProperties(wxUserInfo, item);
		wxUserInfoDao.save(wxUserInfo);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param wxUserInfoId
	 * @return
	 */
	public WxUserInfoItem loadWxUserInfoItem(Long wxUserInfoId) {
		WxUserInfo wxUserInfo = wxUserInfoDao.get(wxUserInfoId);
		return WxUserInfoItem.createItem(wxUserInfo);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxUserInfoInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxUserInfoInfo(List<Long> wxUserInfoIds) {
		for (Long id : wxUserInfoIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2018-01-04 11:30:24
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxUserInfoItem> loadItems() {
		List<WxUserInfo> wxUserInfos = wxUserInfoDao.findByHql("from WxUserInfo wui order by wui.id");
		return WxUserInfoItem.createItems(wxUserInfos);
	}
	
	public List<WxUserInfoItem> getWxUserInfoByCondition(
			WxUserInfoItem condition) {
		List<WxUserInfoItem> items = baseDao.findByNamedQuery("findWxUserInfoByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public WxUserInfo getWxUserInfoByOpenId(String companyCode, String openId) {
		return wxUserInfoDao.findUnique("from WxUserInfo where wxCompany.code=? and openId=?",companyCode,openId);
	}
	@Override
	public void wxUserInfoSign(WxUserInfoItem userInfo) {
		WxUserInfo wxUserInfo = getWxUserInfoByOpenId(userInfo.getCompanyCode(), userInfo.getOpenId());
		if (wxUserInfo==null) {
			wxUserInfo = new WxUserInfo();
			BeanUtils.copyProperties(wxUserInfo, userInfo);
		}
		WxCompany wxCompany = wxCompanyService.getCompanyByCode(userInfo.getCompanyCode());
		if (wxCompany!=null) {
			wxUserInfo.setCompany(wxCompany.getName());
		}
		wxUserInfo.setWxCompany(wxCompany);
		save(wxUserInfo);
	}
	@Override
	public int getSignPersonCount(String companyCode) {
		List<WxUserInfo> signUserInfos = wxUserInfoDao.findByHql("from WxUserInfo where wxCompany.code=? and signStatus=?", companyCode,SysConstants.YES);
		return signUserInfos==null?0:signUserInfos.size();
	}
	@Override
	public void saveImportWxUserInfos(List<WxUserInfoItem> items) {
		if (EmptyUtil.isNotEmpty(items)) {
			WxUserInfo wxUserInfo = null;
			for (WxUserInfoItem wxUserInfoItem : items) {
				if (StringUtils.isNotBlank(wxUserInfoItem.getName())) {
					wxUserInfo = wxUserInfoDao.findUniqueBy("name", wxUserInfoItem.getName().trim());
					if (wxUserInfo==null) {
						wxUserInfo = new WxUserInfo();
					}
					wxUserInfo.setName(wxUserInfoItem.getName().trim());
					wxUserInfo.setHczw(wxUserInfoItem.getHczw());
					wxUserInfo.setYhzw(wxUserInfoItem.getYhzw());
					save(wxUserInfo);
				}
			}
		}
	}
	@Override
	public WxUserInfo getWxUserByName(String name) {
		return wxUserInfoDao.findUniqueBy("name", name.trim());
	}
	@Override
	public WxUserInfoItem getUserInfoByOpenId(String openId) {
		WxUserInfo wxUserInfo = wxUserInfoDao.findUniqueBy("openId", openId);
		return WxUserInfoItem.createItem(wxUserInfo);
	}
	@Override
	public void resetWxUserInfoInfo(List<Long> wxUserInfoIds) {
		WxUserInfo wxUserInfo = null;
		for (Long id : wxUserInfoIds) {
			wxUserInfo = get(id);
			wxUserInfo.setOpenId("");
			wxUserInfo.setNickName("");
			wxUserInfo.setCountry("");
			wxUserInfo.setProvince("");
			wxUserInfo.setCity("");
			wxUserInfo.setHeadImgUrl("");
			wxUserInfo.setPosition("");
			wxUserInfo.setSex("");
			wxUserInfo.setSignStatus(SysConstants.NO);
			save(wxUserInfo);
		}
		
	}

}
