package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxZklyInfoDao;
import com.xdtech.wx.model.WxZklyInfo;
import com.xdtech.wx.service.WxZklyInfoService;
import com.xdtech.wx.vo.WxZklyInfoItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see
 */
@Service
public class WxZklyInfoServiceImpl implements WxZklyInfoService {
	private Log log = LogFactory.getLog(WxZklyInfoServiceImpl.class);
	@Autowired
	private WxZklyInfoDao wxZklyInfoDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param entity
	 */
	public void save(WxZklyInfo entity) {
		wxZklyInfoDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxZklyInfo> entities) {
		wxZklyInfoDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param entity
	 */
	public void delete(WxZklyInfo entity) {
		wxZklyInfoDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxZklyInfoDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxZklyInfo get(Long id) {
		return wxZklyInfoDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @return
	 */
	public List<WxZklyInfo> getAll() {
		return wxZklyInfoDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxZklyInfos = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxZklyInfo> page = wxZklyInfoDao.findPage(pg,"from WxZklyInfo order by createTime desc", values);
			wxZklyInfos = BeanUtils.copyListProperties(
					WxZklyInfoItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxZklyInfo> wxZklyInfoList = wxZklyInfoDao.find("from WxZklyInfo order by id desc", values);
			wxZklyInfos = BeanUtils.copyListProperties(
					WxZklyInfoItem.class, wxZklyInfoList);
			rows = wxZklyInfos.size();
		}
		results.put("total", rows);
		results.put("rows", wxZklyInfos);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxZklyInfo(WxZklyInfoItem item) {
		WxZklyInfo wxZklyInfo = null;
		if (item.getId()==null) {
			wxZklyInfo = new WxZklyInfo();
		}else {
			wxZklyInfo = wxZklyInfoDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxZklyInfo, item);
		wxZklyInfoDao.save(wxZklyInfo);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param wxZklyInfoId
	 * @return
	 */
	public WxZklyInfoItem loadWxZklyInfoItem(Long wxZklyInfoId) {
		WxZklyInfo wxZklyInfo = wxZklyInfoDao.get(wxZklyInfoId);
		return WxZklyInfoItem.createItem(wxZklyInfo);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxZklyInfoInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxZklyInfoInfo(List<Long> wxZklyInfoIds) {
		for (Long id : wxZklyInfoIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxZklyInfoItem> loadItems() {
		List<WxZklyInfo> wxZklyInfos = getAll();
		return WxZklyInfoItem.createItems(wxZklyInfos);
	}
	
	public List<WxZklyInfoItem> getWxZklyInfoByCondition(
			WxZklyInfoItem condition) {
		List<WxZklyInfoItem> items = baseDao.findByNamedQuery("findWxZklyInfoByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public WxZklyInfo getZklyInfoByUUID(String uuid) {
		WxZklyInfo wxZklyInfo = wxZklyInfoDao.findUniqueBy("uuid", uuid);
		return wxZklyInfo;
	}

}
