package com.xdtech.wx.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xdtech.core.dao.BaseDao;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.Page;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.dao.WxPzswSolveDao;
import com.xdtech.wx.model.WxPzswQuestion;
import com.xdtech.wx.model.WxPzswSolve;
import com.xdtech.wx.service.WxPzswSolveService;
import com.xdtech.wx.vo.PzswSolveVo;
import com.xdtech.wx.vo.PzswVo;
import com.xdtech.wx.vo.WxPzswSolveItem;
import com.xdtech.web.model.Pagination;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see
 */
@Service
public class WxPzswSolveServiceImpl implements WxPzswSolveService {
	private Log log = LogFactory.getLog(WxPzswSolveServiceImpl.class);
	@Autowired
	private WxPzswSolveDao wxPzswSolveDao;
	@Autowired
	private BaseDao baseDao;
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param entity
	 */
	public void save(WxPzswSolve entity) {
		wxPzswSolveDao.save(entity);
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param entity
	 */
	public void saveAll(List<WxPzswSolve> entities) {
		wxPzswSolveDao.saveAll(entities);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param entity
	 */
	public void delete(WxPzswSolve entity) {
		wxPzswSolveDao.delete(entity);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param id
	 */
	public void delete(Long id) {
		wxPzswSolveDao.delete(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param id
	 * @return
	 */
	public WxPzswSolve get(Long id) {
		return wxPzswSolveDao.get(id);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @return
	 */
	public List<WxPzswSolve> getAll() {
		return wxPzswSolveDao.getAll();
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param pg
	 * @param values
	 * @return
	 */
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			Map<String, String> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		List<Object> wxPzswSolves = null;
		long rows = 0;
		if (pg!=null) {
			Page<WxPzswSolve> page = wxPzswSolveDao.findPage(pg,"from WxPzswSolve order by createTime desc", values);
			wxPzswSolves = BeanUtils.copyListProperties(
					WxPzswSolveItem.class, page.getResult());
			rows = page.getTotalItems();
		}else {
			List<WxPzswSolve> wxPzswSolveList = wxPzswSolveDao.find("from WxPzswSolve order by id desc", values);
			wxPzswSolves = BeanUtils.copyListProperties(
					WxPzswSolveItem.class, wxPzswSolveList);
			rows = wxPzswSolves.size();
		}
		results.put("total", rows);
		results.put("rows", wxPzswSolves);
		return results;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param item
	 * @return
	 */
	public boolean saveOrUpdateWxPzswSolve(WxPzswSolveItem item) {
		WxPzswSolve wxPzswSolve = null;
		if (item.getId()==null) {
			wxPzswSolve = new WxPzswSolve();
		}else {
			wxPzswSolve = wxPzswSolveDao.get(item.getId());
		}
		//复制前台修改的属性
		BeanUtils.copyProperties(wxPzswSolve, item);
		wxPzswSolveDao.save(wxPzswSolve);
		return true;
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param wxPzswSolveId
	 * @return
	 */
	public WxPzswSolveItem loadWxPzswSolveItem(Long wxPzswSolveId) {
		WxPzswSolve wxPzswSolve = wxPzswSolveDao.get(wxPzswSolveId);
		return WxPzswSolveItem.createItem(wxPzswSolve);
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param id
	 * @return
	 */
	public boolean deleteWxPzswSolveInfo(long id) {
		delete(id);
		return true;
	}
	
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param newIds
	 * @return
	 */
	public boolean deleteWxPzswSolveInfo(List<Long> wxPzswSolveIds) {
		for (Long id : wxPzswSolveIds) {
			delete(id);
		}
		return true;
	}
	
	public Map<String, Object> loadPageDataByConditions(Pagination pg,
			BaseItem baseItem, String queryName) {
		Map<String, Object> results = new HashMap<String, Object>();
		Page page = baseDao.findPageByNamedQuery(pg, queryName,baseItem);
		results.put("total", page.getTotalItems());
		results.put("rows", page.getResult());
		return results;
	}
	/**
	 * @description
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @return
	 */
	@Override
	public List<WxPzswSolveItem> loadItems() {
		List<WxPzswSolve> wxPzswSolves = getAll();
		return WxPzswSolveItem.createItems(wxPzswSolves);
	}
	
	public List<WxPzswSolveItem> getWxPzswSolveByCondition(
			WxPzswSolveItem condition) {
		List<WxPzswSolveItem> items = baseDao.findByNamedQuery("findWxPzswSolveByCondition",condition,condition.loadQueryFields());
		return items;
	}
	@Override
	public void saveInfo(WxPzswQuestion pzswQuestion, PzswVo pzswVo) {
		wxPzswSolveDao.batchExecute("delete from WxPzswSolve where wxPzswQuestion.id=?", pzswQuestion.getId());
		saveSolve(pzswQuestion,pzswVo.getReason1Solve1());
		saveSolve(pzswQuestion,pzswVo.getReason1Solve2());
		saveSolve(pzswQuestion,pzswVo.getReason2Solve1());
		saveSolve(pzswQuestion,pzswVo.getReason2Solve2());
		saveSolve(pzswQuestion,pzswVo.getReason3Solve1());
		saveSolve(pzswQuestion,pzswVo.getReason3Solve2());
	}
	private void saveSolve(WxPzswQuestion pzswQuestion,
			PzswSolveVo reasonSolve) {
		WxPzswSolve pzswSolve = new WxPzswSolve();
		pzswSolve.setCreateTime(new Date());
		pzswSolve.setName(reasonSolve.getName());
		pzswSolve.setChoose(reasonSolve.getChoose());
		pzswSolve.setWxPzswQuestion(pzswQuestion);
		save(pzswSolve);
	}

}
