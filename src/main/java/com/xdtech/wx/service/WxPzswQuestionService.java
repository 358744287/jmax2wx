package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxPzswQuestion;
import com.xdtech.wx.vo.PzswVo;
import com.xdtech.wx.vo.WxPzswQuestionItem;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see
 */
public interface WxPzswQuestionService extends IBaseService<WxPzswQuestion>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxPzswQuestion(WxPzswQuestionItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxPzswQuestionItem loadWxPzswQuestionItem(Long wxPzswQuestionId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxPzswQuestionInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @param wxPzswQuestionIds
	 */
	boolean deleteWxPzswQuestionInfo(List<Long> wxPzswQuestionIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @return
	 */
	List<WxPzswQuestionItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-05-08 14:17:02
	 * @modified by
	 * @return
	 */
	public List<WxPzswQuestionItem> getWxPzswQuestionByCondition(
			WxPzswQuestionItem condition);

	Long saveSubmitInfo(PzswVo pzswVo);

	void saveImgUrl(Long pzswId, String imgUrl);
}
