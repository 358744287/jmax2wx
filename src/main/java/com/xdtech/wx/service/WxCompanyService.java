package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxCompany;
import com.xdtech.wx.vo.WxCompanyItem;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see
 */
public interface WxCompanyService extends IBaseService<WxCompany>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxCompany(WxCompanyItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxCompanyItem loadWxCompanyItem(Long wxCompanyId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxCompanyInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @param wxCompanyIds
	 */
	boolean deleteWxCompanyInfo(List<Long> wxCompanyIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @return
	 */
	List<WxCompanyItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2018-01-04 11:30:21
	 * @modified by
	 * @return
	 */
	public List<WxCompanyItem> getWxCompanyByCondition(
			WxCompanyItem condition);

	WxCompany getCompanyByCode(String companyCode);
	/**
	 * 根据公司编码获取公司对应的企业人数
	 * @param companyCode
	 * @return
	 */
	int getPersonCount(String companyCode);
}
