package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxZklyInfo;
import com.xdtech.wx.vo.WxZklyInfoItem;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see
 */
public interface WxZklyInfoService extends IBaseService<WxZklyInfo>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxZklyInfo(WxZklyInfoItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxZklyInfoItem loadWxZklyInfoItem(Long wxZklyInfoId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxZklyInfoInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @param wxZklyInfoIds
	 */
	boolean deleteWxZklyInfoInfo(List<Long> wxZklyInfoIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @return
	 */
	List<WxZklyInfoItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-09-14 23:28:40
	 * @modified by
	 * @return
	 */
	public List<WxZklyInfoItem> getWxZklyInfoByCondition(
			WxZklyInfoItem condition);

	WxZklyInfo getZklyInfoByUUID(String uuid);
}
