package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxPzswQuestion;
import com.xdtech.wx.model.WxPzswSolve;
import com.xdtech.wx.vo.PzswVo;
import com.xdtech.wx.vo.WxPzswSolveItem;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see
 */
public interface WxPzswSolveService extends IBaseService<WxPzswSolve>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxPzswSolve(WxPzswSolveItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxPzswSolveItem loadWxPzswSolveItem(Long wxPzswSolveId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxPzswSolveInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @param wxPzswSolveIds
	 */
	boolean deleteWxPzswSolveInfo(List<Long> wxPzswSolveIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @return
	 */
	List<WxPzswSolveItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-05-08 14:17:05
	 * @modified by
	 * @return
	 */
	public List<WxPzswSolveItem> getWxPzswSolveByCondition(
			WxPzswSolveItem condition);

	void saveInfo(WxPzswQuestion pzswQuestion, PzswVo pzswVo);
}
