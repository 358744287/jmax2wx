package com.xdtech.wx.service;

import java.util.List;

import com.xdtech.common.service.IBaseService;
import com.xdtech.wx.model.WxZklyDay;
import com.xdtech.wx.vo.WxZklyDayItem;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see
 */
public interface WxZklyDayService extends IBaseService<WxZklyDay>{

	/**
	 * 保存更新信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param item
	 * @return
	 */
	boolean saveOrUpdateWxZklyDay(WxZklyDayItem item);

	/**
	 * 加载记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param newId
	 * @return
	 */
	WxZklyDayItem loadWxZklyDayItem(Long wxZklyDayId);

	/**
	 * 根据id号删除记录信息
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param id
	 * @return
	 */
	boolean deleteWxZklyDayInfo(long id);

	/**
	 * 
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @param wxZklyDayIds
	 */
	boolean deleteWxZklyDayInfo(List<Long> wxZklyDayIds);
	
	/**
	 * 加载vo列表条目
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @return
	 */
	List<WxZklyDayItem> loadItems();
	/**
	 * 根据查询器直接查询符合的数据集合，不分页
	 * @author max.zheng
	 * @create 2017-09-22 00:26:27
	 * @modified by
	 * @return
	 */
	public List<WxZklyDayItem> getWxZklyDayByCondition(
			WxZklyDayItem condition);
}
