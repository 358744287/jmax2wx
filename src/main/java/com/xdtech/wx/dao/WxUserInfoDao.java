package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxUserInfo;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:24
 * @since 1.0
 * @see
 */
@Repository
public class WxUserInfoDao extends HibernateDao<WxUserInfo, Long>{

}
