package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WeixinResponse;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see
 */
@Repository
public class WeixinResponseDao extends HibernateDao<WeixinResponse, Long>{

}
