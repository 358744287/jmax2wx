package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxZklyDay;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see
 */
@Repository
public class WxZklyDayDao extends HibernateDao<WxZklyDay, Long>{

}
