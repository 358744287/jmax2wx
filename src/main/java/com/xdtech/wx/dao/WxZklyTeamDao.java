package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxZklyTeam;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see
 */
@Repository
public class WxZklyTeamDao extends HibernateDao<WxZklyTeam, Long>{

}
