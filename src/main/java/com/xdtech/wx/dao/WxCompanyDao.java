package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxCompany;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see
 */
@Repository
public class WxCompanyDao extends HibernateDao<WxCompany, Long>{

}
