package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxPzswQuestion;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see
 */
@Repository
public class WxPzswQuestionDao extends HibernateDao<WxPzswQuestion, Long>{

}
