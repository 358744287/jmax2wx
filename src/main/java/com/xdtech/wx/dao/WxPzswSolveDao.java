package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxPzswSolve;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see
 */
@Repository
public class WxPzswSolveDao extends HibernateDao<WxPzswSolve, Long>{

}
