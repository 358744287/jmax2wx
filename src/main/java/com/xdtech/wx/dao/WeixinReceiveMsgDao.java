package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WeixinReceiveMsg;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see
 */
@Repository
public class WeixinReceiveMsgDao extends HibernateDao<WeixinReceiveMsg, Long>{

}
