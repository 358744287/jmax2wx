package com.xdtech.wx.dao;

import org.springframework.stereotype.Repository;

import com.xdtech.core.orm.hibernate.HibernateDao;
import com.xdtech.wx.model.WxZklyInfo;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see
 */
@Repository
public class WxZklyInfoDao extends HibernateDao<WxZklyInfo, Long>{

}
