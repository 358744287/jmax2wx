package com.xdtech.wx.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;

import com.xdtech.common.utils.ApplicationContextUtil;

import net.sf.json.JSONObject;

public class WeixinUtil {

	/**
	 * 方法名：httpRequest</br> 详述：发送http请求</br> 开发人员：souvc </br> 创建时间：2016-1-5
	 * </br>
	 * 
	 * @param requestUrl
	 * @param requestMethod
	 * @param outputStr
	 * @return 说明返回值含义
	 * @throws 说明发生此异常的条件
	 */
	public static JSONObject httpRequest(String requestUrl,
			String requestMethod, String outputStr) {
		JSONObject jsonObject = null;
		StringBuffer buffer = new StringBuffer();
		try {
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url
					.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);
			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			httpUrlConn.setRequestMethod(requestMethod);
			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);
			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			jsonObject = JSONObject.fromObject(buffer.toString());
		} catch (ConnectException ce) {
			ce.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	public static String getRandomString(Integer length) {
		String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}
	/**
	 * 方法名：getWxConfig</br> 详述：获取微信的配置信息 </br> 开发人员：souvc </br> 创建时间：2016-1-5
	 * </br>
	 * wx44fdc31fc571757d
	 * 9cb6776d0305735194574d2118df7df3
	 * @param request
	 * @return 说明返回值含义
	 * @throws 说明发生此异常的条件
	 */
	public static Map<String, String> getWxConfig(String url2) {
		Map<String, String> ret = new HashMap<String, String>();

//		String appId = "wxb7db1ebdb7ef8470"; // 必填，公众号的唯一标识
//		String secret = "d7ccb5cb4a463826e477695357a0461e";
		
		String appId = "wx44fdc31fc571757d"; // 必填，公众号的唯一标识
		String secret = "9cb6776d0305735194574d2118df7df3";

//		sapi_ticket这里的ticket是需要两次请求得到的，不是access_token！我这里就调到坑里了，关键的说明微信的字很小，我给忽视了~
//		先获取access_token：https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+WxConfig.appid+"&secret="+WxConfig.appsecret;
//		在用access_token获取ticket：https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+accessToken+"&type=jsapi"
		
		String requestUrl = url2;
		String access_token = "";
		String jsapi_ticket = "";
		String timestamp = Long.toString(System.currentTimeMillis() / 1000); // 必填，生成签名的时间戳
		String nonceStr = getRandomString(32);//UUID.randomUUID().toString(); // 必填，生成签名的随机串
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
				+ appId + "&secret=" + secret;

		JSONObject json = httpRequest(url, "GET", null);

		if (json != null) {
			// 要注意，access_token需要缓存
			access_token = json.getString("access_token");

			url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="
					+ access_token + "&type=jsapi";
			json = WeixinUtil.httpRequest(url, "GET", null);
			if (json != null) {
				jsapi_ticket = json.getString("ticket");
//				System.out.println("ticket="+jsapi_ticket);
			}
		}
		String signature = "";
		// 注意这里参数名必须全部小写，且必须有序
		String sign = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + nonceStr
				+ "&timestamp=" + timestamp + "&url=" + requestUrl;
		System.out.println(sign);
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(sign.getBytes("UTF-8"));
			signature = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		ret.put("appId", appId);
		ret.put("timestamp", timestamp);
		ret.put("nonceStr", nonceStr);
		ret.put("signature", signature);//7fb6f0d08abc9fca99b9b6e400b7e2c35dd3004c
		System.out.println("signature="+signature);

		return ret;
	}

	/**
	 * 方法名：byteToHex</br> 详述：字符串加密辅助方法 </br> 开发人员：souvc </br> 创建时间：2016-1-5
	 * </br>
	 * 
	 * @param hash
	 * @return 说明返回值含义
	 * @throws 说明发生此异常的条件
	 */
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;

	}
	
	public static void main(String[] args) {
		getWxConfig("http://www.jmax4you.com/jmax/pzsw.action?page=index4");
	}
}
