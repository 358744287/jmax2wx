package com.xdtech.wx.util;

public class Token {
	private String accessToken;
	private Integer expiresIn;
	
	public Token() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Token(String accessToken, Integer expiresIn) {
		super();
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Integer getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
	
	
}
