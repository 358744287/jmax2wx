package com.xdtech.wx.vo;

import java.io.Serializable;

public class PzswSolveVo implements Serializable{
	private static final long serialVersionUID = -4145048093929919998L;

	private Integer id;
	private String name;
	private String choose = "false";
	
	
	public PzswSolveVo() {
		super();
	}
	
	
	public PzswSolveVo(Integer id) {
		super();
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	


	public String getChoose() {
		return choose;
	}


	public void setChoose(String choose) {
		this.choose = choose;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}
	
}
