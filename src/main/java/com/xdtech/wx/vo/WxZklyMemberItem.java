package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxZklyMember;
import com.xdtech.wx.model.WxZklyTeam;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:24
 * @since 1.0
 * @see
 */
public class WxZklyMemberItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="姓名",width=100)
	private String name;
	@GridColumn(title="专业",width=100)
	private String major;
	@GridColumn(title="头像",width=100)
	private String imgUrl;
	
	private Long wxZklyTeamId;

	
	public WxZklyMemberItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public WxZklyMemberItem(Long wxZklyTeamId) {
		super();
		this.wxZklyTeamId = wxZklyTeamId;
	}

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setMajor(String major) {
		this.major = major;
		addQuerys("major", major);
	}
	public String getMajor() {
		return major;
	}
	
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
		addQuerys("imgUrl", imgUrl);
	}
	public String getImgUrl() {
		return imgUrl;
	}
	
	public Long getWxZklyTeamId() {
		return wxZklyTeamId;
	}
	public void setWxZklyTeamId(Long wxZklyTeamId) {
		this.wxZklyTeamId = wxZklyTeamId;
		addQuerys("wxZklyTeamId", wxZklyTeamId);
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxZklyMemberItem createItem(WxZklyMember wxZklyMember) {
		WxZklyMemberItem wxZklyMemberItem = null;
		if(wxZklyMember!=null) {
			wxZklyMemberItem = new WxZklyMemberItem();
			BeanUtils.copyProperties(wxZklyMemberItem, wxZklyMember);
			//自定义属性设置填充
			WxZklyTeam wxZklyTeam = wxZklyMember.getWxZklyTeam();
			if (wxZklyTeam!=null) {
				wxZklyMemberItem.setWxZklyTeamId(wxZklyTeam.getId());
			}
		}
		
		return wxZklyMemberItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxZklyMemberItem> createItems(List<WxZklyMember> wxZklyMembers) {
		List<WxZklyMemberItem> wxZklyMemberItems = new ArrayList<WxZklyMemberItem>();
		for (WxZklyMember wxZklyMember : wxZklyMembers) {
			wxZklyMemberItems.add(createItem(wxZklyMember));
		}
		return wxZklyMemberItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxZklyMember toModel() {
		WxZklyMember model = new WxZklyMember();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
