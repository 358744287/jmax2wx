package com.xdtech.wx.vo;

import java.io.Serializable;

public class PzswVo implements Serializable{
	private static final long serialVersionUID = 1L;
	//max
	private String name;
	//研发部
	private String dept;
	//不安全现象
	private String moduleType;
	
	//走路看手机
	private String questionName;
	//问题原因1
	private String questionReason1;
	//问题原因2
	private String questionReason2;
	//问题原因3
	private String questionReason3;
	//原因1 解决方案
	private PzswSolveVo reason1Solve1 = new PzswSolveVo(11);
	private PzswSolveVo reason1Solve2 = new PzswSolveVo(12);
	//原因2 解决方案
	private PzswSolveVo reason2Solve1 = new PzswSolveVo(21);
	private PzswSolveVo reason2Solve2 = new PzswSolveVo(22);
	//原因3 解决方案
	private PzswSolveVo reason3Solve1 = new PzswSolveVo(31);
	private PzswSolveVo reason3Solve2 = new PzswSolveVo(32);
	
	private Long id;
	
	public PzswVo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PzswVo(String name, String dept, String moduleType) {
		super();
		this.name = name;
		this.dept = dept;
		this.moduleType = moduleType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQuestionReason1() {
		return questionReason1;
	}
	public void setQuestionReason1(String questionReason1) {
		this.questionReason1 = questionReason1;
	}
	public String getQuestionReason2() {
		return questionReason2;
	}
	public void setQuestionReason2(String questionReason2) {
		this.questionReason2 = questionReason2;
	}
	public String getQuestionReason3() {
		return questionReason3;
	}
	public void setQuestionReason3(String questionReason3) {
		this.questionReason3 = questionReason3;
	}
	public PzswSolveVo getReason1Solve1() {
		return reason1Solve1;
	}
	public void setReason1Solve1(PzswSolveVo reason1Solve1) {
		this.reason1Solve1 = reason1Solve1;
	}
	public PzswSolveVo getReason1Solve2() {
		return reason1Solve2;
	}
	public void setReason1Solve2(PzswSolveVo reason1Solve2) {
		this.reason1Solve2 = reason1Solve2;
	}
	public PzswSolveVo getReason2Solve1() {
		return reason2Solve1;
	}
	public void setReason2Solve1(PzswSolveVo reason2Solve1) {
		this.reason2Solve1 = reason2Solve1;
	}
	public PzswSolveVo getReason3Solve1() {
		return reason3Solve1;
	}
	public PzswSolveVo getReason2Solve2() {
		return reason2Solve2;
	}
	public void setReason2Solve2(PzswSolveVo reason2Solve2) {
		this.reason2Solve2 = reason2Solve2;
	}
	public void setReason3Solve1(PzswSolveVo reason3Solve1) {
		this.reason3Solve1 = reason3Solve1;
	}
	public PzswSolveVo getReason3Solve2() {
		return reason3Solve2;
	}
	public void setReason3Solve2(PzswSolveVo reason3Solve2) {
		this.reason3Solve2 = reason3Solve2;
	}
	
}
