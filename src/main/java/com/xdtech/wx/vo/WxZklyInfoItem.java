package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxZklyInfo;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see
 */
public class WxZklyInfoItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	@GridColumn(title="宣言",width=100)
	private String speak;
	@GridColumn(title="院系",width=100)
	private String school;
	@GridColumn(title="年级",width=100)
	private String gradeLevel;
	private Long id;
	@GridColumn(title="名称",width=100)
	private String name;
	@GridColumn(title="专业",width=100)
	private String major;
	@GridColumn(title="宿舍门牌",width=100)
	private String dorm;
	@GridColumn(title="uuid",width=100)
	private String uuid;
	@GridColumn(title="照片路径",width=100)
	private String imgUrl;
	@GridColumn(title="电话",width=100)
	private String telphone;
	@GridColumn(title="身份证号",width=100)
	private String identityNo;

	public void setSpeak(String speak) {
		this.speak = speak;
		addQuerys("speak", speak);
	}
	public String getSpeak() {
		return speak;
	}
	
	public void setSchool(String school) {
		this.school = school;
		addQuerys("school", school);
	}
	public String getSchool() {
		return school;
	}
	
	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
		addQuerys("gradeLevel", gradeLevel);
	}
	public String getGradeLevel() {
		return gradeLevel;
	}
	
	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setMajor(String major) {
		this.major = major;
		addQuerys("major", major);
	}
	public String getMajor() {
		return major;
	}
	
	public void setDorm(String dorm) {
		this.dorm = dorm;
		addQuerys("dorm", dorm);
	}
	public String getDorm() {
		return dorm;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
		addQuerys("uuid", uuid);
	}
	public String getUuid() {
		return uuid;
	}
	
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
		addQuerys("imgUrl", imgUrl);
	}
	public String getImgUrl() {
		return imgUrl;
	}
	
	public void setTelphone(String telphone) {
		this.telphone = telphone;
		addQuerys("telphone", telphone);
	}
	public String getTelphone() {
		return telphone;
	}
	
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
		addQuerys("identityNo", identityNo);
	}
	public String getIdentityNo() {
		return identityNo;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxZklyInfoItem createItem(WxZklyInfo wxZklyInfo) {
		WxZklyInfoItem wxZklyInfoItem = null;
		if(wxZklyInfo!=null) {
			wxZklyInfoItem = new WxZklyInfoItem();
			BeanUtils.copyProperties(wxZklyInfoItem, wxZklyInfo);
			//自定义属性设置填充
		}
		
		return wxZklyInfoItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxZklyInfoItem> createItems(List<WxZklyInfo> wxZklyInfos) {
		List<WxZklyInfoItem> wxZklyInfoItems = new ArrayList<WxZklyInfoItem>();
		for (WxZklyInfo wxZklyInfo : wxZklyInfos) {
			wxZklyInfoItems.add(createItem(wxZklyInfo));
		}
		return wxZklyInfoItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxZklyInfo toModel() {
		WxZklyInfo model = new WxZklyInfo();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
