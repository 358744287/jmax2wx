package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxPzswQuestion;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see
 */
public class WxPzswQuestionItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="部门/TM",width=100)
	private String dept;
	@GridColumn(title="姓名/油站",width=100)
	private String name;
	@GridColumn(title="问题类型",width=100)
	private String moduleType;
	@GridColumn(title="问题名称",width=100)
	private String questionName;
	@GridColumn(title="原因1",width=100)
	private String questionReason1;
	@GridColumn(title="原因2",width=100)
	private String questionReason2;
	@GridColumn(title="原因3",width=100)
	private String questionReason3;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setDept(String dept) {
		this.dept = dept;
		addQuerys("dept", dept);
	}
	public String getDept() {
		return dept;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
		addQuerys("moduleType", moduleType);
	}
	public String getModuleType() {
		return moduleType;
	}
	
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
		addQuerys("questionName", questionName);
	}
	public String getQuestionName() {
		return questionName;
	}
	
	public void setQuestionReason1(String questionReason1) {
		this.questionReason1 = questionReason1;
		addQuerys("questionReason1", questionReason1);
	}
	public String getQuestionReason1() {
		return questionReason1;
	}
	
	public void setQuestionReason2(String questionReason2) {
		this.questionReason2 = questionReason2;
		addQuerys("questionReason2", questionReason2);
	}
	public String getQuestionReason2() {
		return questionReason2;
	}
	
	public void setQuestionReason3(String questionReason3) {
		this.questionReason3 = questionReason3;
		addQuerys("questionReason3", questionReason3);
	}
	public String getQuestionReason3() {
		return questionReason3;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxPzswQuestionItem createItem(WxPzswQuestion wxPzswQuestion) {
		WxPzswQuestionItem wxPzswQuestionItem = null;
		if(wxPzswQuestion!=null) {
			wxPzswQuestionItem = new WxPzswQuestionItem();
			BeanUtils.copyProperties(wxPzswQuestionItem, wxPzswQuestion);
			//自定义属性设置填充
		}
		
		return wxPzswQuestionItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxPzswQuestionItem> createItems(List<WxPzswQuestion> wxPzswQuestions) {
		List<WxPzswQuestionItem> wxPzswQuestionItems = new ArrayList<WxPzswQuestionItem>();
		for (WxPzswQuestion wxPzswQuestion : wxPzswQuestions) {
			wxPzswQuestionItems.add(createItem(wxPzswQuestion));
		}
		return wxPzswQuestionItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxPzswQuestion toModel() {
		WxPzswQuestion model = new WxPzswQuestion();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
