package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxPzswSolve;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see
 */
public class WxPzswSolveItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="方案描述",width=100)
	private String name;
	@GridColumn(title="是否选中",width=100)
	private String select;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setSelect(String select) {
		this.select = select;
		addQuerys("select", select);
	}
	public String getSelect() {
		return select;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxPzswSolveItem createItem(WxPzswSolve wxPzswSolve) {
		WxPzswSolveItem wxPzswSolveItem = null;
		if(wxPzswSolve!=null) {
			wxPzswSolveItem = new WxPzswSolveItem();
			BeanUtils.copyProperties(wxPzswSolveItem, wxPzswSolve);
			//自定义属性设置填充
		}
		
		return wxPzswSolveItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxPzswSolveItem> createItems(List<WxPzswSolve> wxPzswSolves) {
		List<WxPzswSolveItem> wxPzswSolveItems = new ArrayList<WxPzswSolveItem>();
		for (WxPzswSolve wxPzswSolve : wxPzswSolves) {
			wxPzswSolveItems.add(createItem(wxPzswSolve));
		}
		return wxPzswSolveItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxPzswSolve toModel() {
		WxPzswSolve model = new WxPzswSolve();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
