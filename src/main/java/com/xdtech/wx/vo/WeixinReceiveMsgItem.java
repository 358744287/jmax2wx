package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WeixinReceiveMsg;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see
 */
public class WeixinReceiveMsgItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="开发者微信号",width=100)
	private String toUserName;
	@GridColumn(title="发送方账号（OpenId）",width=100)
	private String fromUserName;
	@GridColumn(title="消息类型",width=100)
	private String msgType;
	@GridColumn(title="消息id，64位整型",width=100)
	private String msgId;
	@GridColumn(title="消息内容",width=100)
	private String content;
	@GridColumn(title="消息回复",width=100)
	private String response;
	@GridColumn(title="用户昵称",width=100)
	private String nickName;
//	@GridColumn(title="微信账号ID",width=100)
	private String accountId;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
		addQuerys("toUserName", toUserName);
	}
	public String getToUserName() {
		return toUserName;
	}
	
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
		addQuerys("fromUserName", fromUserName);
	}
	public String getFromUserName() {
		return fromUserName;
	}
	
	public void setMsgType(String msgType) {
		this.msgType = msgType;
		addQuerys("msgType", msgType);
	}
	public String getMsgType() {
		return msgType;
	}
	
	public void setMsgId(String msgId) {
		this.msgId = msgId;
		addQuerys("msgId", msgId);
	}
	public String getMsgId() {
		return msgId;
	}
	
	public void setContent(String content) {
		this.content = content;
		addQuerys("content", content);
	}
	public String getContent() {
		return content;
	}
	
	public void setResponse(String response) {
		this.response = response;
		addQuerys("response", response);
	}
	public String getResponse() {
		return response;
	}
	
	public void setNickName(String nickName) {
		this.nickName = nickName;
		addQuerys("nickName", nickName);
	}
	public String getNickName() {
		return nickName;
	}
	
	
	
	public String getAccountId()
	{
		return accountId;
	}
	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WeixinReceiveMsgItem createItem(WeixinReceiveMsg weixinReceiveMsg) {
		WeixinReceiveMsgItem weixinReceiveMsgItem = null;
		if(weixinReceiveMsg!=null) {
			weixinReceiveMsgItem = new WeixinReceiveMsgItem();
			BeanUtils.copyProperties(weixinReceiveMsgItem, weixinReceiveMsg);
			//自定义属性设置填充
		}
		
		return weixinReceiveMsgItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WeixinReceiveMsgItem> createItems(List<WeixinReceiveMsg> weixinReceiveMsgs) {
		List<WeixinReceiveMsgItem> weixinReceiveMsgItems = new ArrayList<WeixinReceiveMsgItem>();
		for (WeixinReceiveMsg weixinReceiveMsg : weixinReceiveMsgs) {
			weixinReceiveMsgItems.add(createItem(weixinReceiveMsg));
		}
		return weixinReceiveMsgItems;
	}
}
