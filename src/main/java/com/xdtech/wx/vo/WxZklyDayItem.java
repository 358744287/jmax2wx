package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxZklyDay;
import com.xdtech.wx.model.WxZklyTeam;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see
 */
public class WxZklyDayItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="时间",width=100)
	private String dayTime;
	@GridColumn(title="销量数",width=100)
	private Integer dayCount;
	
	private Long wxZklyTeamId;
	
	private String showTag;

	public WxZklyDayItem() {
		super();
	}
	public WxZklyDayItem(Long wxZklyTeamId) {
		super();
		this.wxZklyTeamId = wxZklyTeamId;
	}
	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setDayTime(String dayTime) {
		this.dayTime = dayTime;
		addQuerys("dayTime", dayTime);
	}
	public String getDayTime() {
		return dayTime;
	}
	
	public void setDayCount(Integer dayCount) {
		this.dayCount = dayCount;
		addQuerys("dayCount", dayCount);
	}
	public Integer getDayCount() {
		return dayCount;
	}
	
	public Long getWxZklyTeamId() {
		return wxZklyTeamId;
	}
	public void setWxZklyTeamId(Long wxZklyTeamId) {
		this.wxZklyTeamId = wxZklyTeamId;
		addQuerys("wxZklyTeamId", wxZklyTeamId);
	}
	public String getShowTag() {
		String[] dayArray = this.dayTime.split("-");
		this.showTag = dayArray[1]+"/"+dayArray[2];
		return showTag;
	}
	public void setShowTag(String showTag) {
		this.showTag = showTag;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxZklyDayItem createItem(WxZklyDay wxZklyDay) {
		WxZklyDayItem wxZklyDayItem = null;
		if(wxZklyDay!=null) {
			wxZklyDayItem = new WxZklyDayItem();
			BeanUtils.copyProperties(wxZklyDayItem, wxZklyDay);
			//自定义属性设置填充
			WxZklyTeam wxZklyTeam = wxZklyDay.getWxZklyTeam();
			if (wxZklyTeam!=null) {
				wxZklyDayItem.setWxZklyTeamId(wxZklyTeam.getId());
			}
		}
		
		return wxZklyDayItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxZklyDayItem> createItems(List<WxZklyDay> wxZklyDays) {
		List<WxZklyDayItem> wxZklyDayItems = new ArrayList<WxZklyDayItem>();
		for (WxZklyDay wxZklyDay : wxZklyDays) {
			wxZklyDayItems.add(createItem(wxZklyDay));
		}
		return wxZklyDayItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxZklyDay toModel() {
		WxZklyDay model = new WxZklyDay();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
