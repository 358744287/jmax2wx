package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import javax.persistence.Column;

import com.xdtech.common.annotation.Excel;
import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxUserInfo;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:24
 * @since 1.0
 * @see
 */
public class WxUserInfoItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="openId",width=100,show=false)
	private String openId;
	@GridColumn(title="姓名",width=100,enableExport=true)
	@Excel(exportName = "姓名", exportFieldWidth = 20, exportConvertSign = 0, importConvertSign = 0)
	private String name;
	@GridColumn(title="会场座位",width=100,enableExport=true)
	@Excel(exportName = "会场座位", exportFieldWidth = 20, exportConvertSign = 0, importConvertSign = 0)
	private String hczw;
	@GridColumn(title="晚宴座位",width=100,enableExport=true)
	@Excel(exportName = "晚宴座位", exportFieldWidth = 20, exportConvertSign = 0, importConvertSign = 0)
	private String yhzw;
	@GridColumn(title="工作单位",width=100,enableExport=true)
	private String position;
//	@GridColumn(title="工作单位",width=100)
//	@Excel(exportName = "工作单位", exportFieldWidth = 20, exportConvertSign = 0, importConvertSign = 0)
	private String position2;
	@GridColumn(title="昵称",width=100,enableExport=true)
	private String nickName;
	@GridColumn(title="是否签到",width=100,dictionaryCode="YES_OR_NO",enableExport=true)
	private String signStatus;
	@GridColumn(title="性别",width=100,show=false)
	private String sex;
	@GridColumn(title="国家",width=100,show=false)
	private String country;
	@GridColumn(title="省份",width=100,show=false)
	private String province;
	@GridColumn(title="城市",width=100,show=false)
	private String city;
	@GridColumn(title="头像地址",width=100,show=false)
	private String headImgUrl;
	@GridColumn(title="公司",width=100,show=false)
	private String company;
	@GridColumn(title="联系电话",width=100,show=false)
//	@Excel(exportName = "联系电话", exportFieldWidth = 20, exportConvertSign = 0, importConvertSign = 0)
	private String tel;
	
	
	private String companyCode;
	
	private String flag;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public String getHczw() {
		return hczw;
	}
	public void setHczw(String hczw) {
		this.hczw = hczw;
	}
	public String getYhzw() {
		return yhzw;
	}
	public void setYhzw(String yhzw) {
		this.yhzw = yhzw;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
		addQuerys("openId", openId);
	}
	public String getOpenId() {
		return openId;
	}
	
	public void setNickName(String nickName) {
		this.nickName = nickName;
		addQuerys("nickName", nickName);
	}
	public String getNickName() {
		return nickName;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
		addQuerys("sex", sex);
	}
	public String getSex() {
		return sex;
	}
	
	public String getPosition2() {
		return position2;
	}
	public void setPosition2(String position2) {
		this.position2 = position2;
		addQuerys("position2", position2);
	}
	public void setCountry(String country) {
		this.country = country;
		addQuerys("country", country);
	}
	public String getCountry() {
		return country;
	}
	
	public void setProvince(String province) {
		this.province = province;
		addQuerys("province", province);
	}
	public String getProvince() {
		return province;
	}
	
	public void setCity(String city) {
		this.city = city;
		addQuerys("city", city);
	}
	public String getCity() {
		return city;
	}
	
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
		addQuerys("headImgUrl", headImgUrl);
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	
	public void setCompany(String company) {
		this.company = company;
		addQuerys("company", company);
	}
	public String getCompany() {
		return company;
	}
	
	public void setPosition(String position) {
		this.position = position;
		addQuerys("position", position);
	}
	public String getPosition() {
		return position;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setTel(String tel) {
		this.tel = tel;
		addQuerys("tel", tel);
	}
	public String getTel() {
		return tel;
	}
	
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getSignStatus() {
		return signStatus;
	}
	public void setSignStatus(String signStatus) {
		this.signStatus = signStatus;
		addQuerys("signStatus", signStatus);
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxUserInfoItem createItem(WxUserInfo wxUserInfo) {
		WxUserInfoItem wxUserInfoItem = null;
		if(wxUserInfo!=null) {
			wxUserInfoItem = new WxUserInfoItem();
			BeanUtils.copyProperties(wxUserInfoItem, wxUserInfo);
			//自定义属性设置填充
		}
		
		return wxUserInfoItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxUserInfoItem> createItems(List<WxUserInfo> wxUserInfos) {
		List<WxUserInfoItem> wxUserInfoItems = new ArrayList<WxUserInfoItem>();
		for (WxUserInfo wxUserInfo : wxUserInfos) {
			wxUserInfoItems.add(createItem(wxUserInfo));
		}
		return wxUserInfoItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxUserInfo toModel() {
		WxUserInfo model = new WxUserInfo();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
