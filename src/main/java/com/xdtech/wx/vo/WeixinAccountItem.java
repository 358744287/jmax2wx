package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WeixinAccount;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2016-03-10 22:36:32
 * @since 1.0
 * @see
 */
public class WeixinAccountItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="公众号名称",width=100)
	private String name;
	@GridColumn(title="公众号token",width=100)
	private String token;
	@GridColumn(title="公众微信号",width=100)
	private String num;
	@GridColumn(title="公众号原始ID",width=100)
	private String originalId;
	@GridColumn(title="公众号类型",width=100)
	private String accountType;
	@GridColumn(title="邮箱",width=100)
	private String email;
	@GridColumn(title="公众号账号APPID",width=100)
	private String appId;
	@GridColumn(title="描述",width=100)
	private String accountDesc;
	@GridColumn(title="名称",width=100)
	private String appSecret;
//	@GridColumn(title="允许token",width=100)
	private String accessToken;
//	@GridColumn(title="获取token时间",width=100)
	private String setTokenTime;
	@GridColumn(title="系统用户名",width=100)
	private String userName;
	
	private String welcomeText;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setToken(String token) {
		this.token = token;
		addQuerys("token", token);
	}
	public String getToken() {
		return token;
	}
	
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
		addQuerys("num", num);
	}
	public void setOriginalId(String originalId) {
		this.originalId = originalId;
		addQuerys("originalId", originalId);
	}
	public String getOriginalId() {
		return originalId;
	}
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
		addQuerys("accountType", accountType);
	}
	public String getAccountType() {
		return accountType;
	}
	
	public void setEmail(String email) {
		this.email = email;
		addQuerys("email", email);
	}
	public String getEmail() {
		return email;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
		addQuerys("appId", appId);
	}
	public String getAppId() {
		return appId;
	}
	
	public void setAccountDesc(String accountDesc) {
		this.accountDesc = accountDesc;
		addQuerys("accountDesc", accountDesc);
	}
	public String getAccountDesc() {
		return accountDesc;
	}
	
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
		addQuerys("appSecret", appSecret);
	}
	public String getAppSecret() {
		return appSecret;
	}
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		addQuerys("accessToken", accessToken);
	}
	public String getAccessToken() {
		return accessToken;
	}
	
	public void setSetTokenTime(String setTokenTime) {
		this.setTokenTime = setTokenTime;
		addQuerys("setTokenTime", setTokenTime);
	}
	public String getSetTokenTime() {
		return setTokenTime;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
		addQuerys("userName", userName);
	}
	public String getUserName() {
		return userName;
	}
	
	public String getWelcomeText()
	{
		return welcomeText;
	}
	public void setWelcomeText(String welcomeText)
	{
		this.welcomeText = welcomeText;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WeixinAccountItem createItem(WeixinAccount weixinAccount) {
		WeixinAccountItem weixinAccountItem = null;
		if(weixinAccount!=null) {
			weixinAccountItem = new WeixinAccountItem();
			BeanUtils.copyProperties(weixinAccountItem, weixinAccount);
			//自定义属性设置填充
		}
		
		return weixinAccountItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WeixinAccountItem> createItems(List<WeixinAccount> weixinAccounts) {
		List<WeixinAccountItem> weixinAccountItems = new ArrayList<WeixinAccountItem>();
		for (WeixinAccount weixinAccount : weixinAccounts) {
			weixinAccountItems.add(createItem(weixinAccount));
		}
		return weixinAccountItems;
	}
}
