package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxCompany;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see
 */
public class WxCompanyItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="编码",width=100)
	private String code;
	@GridColumn(title="名称",width=100)
	private String name;
	@GridColumn(title="企业人数",width=100)
	private Integer personCount;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setCode(String code) {
		this.code = code;
		addQuerys("code", code);
	}
	public String getCode() {
		return code;
	}
	
	public void setName(String name) {
		this.name = name;
		addQuerys("name", name);
	}
	public String getName() {
		return name;
	}
	
	public void setPersonCount(Integer personCount) {
		this.personCount = personCount;
		addQuerys("personCount", personCount);
	}
	public Integer getPersonCount() {
		return personCount;
	}
	
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxCompanyItem createItem(WxCompany wxCompany) {
		WxCompanyItem wxCompanyItem = null;
		if(wxCompany!=null) {
			wxCompanyItem = new WxCompanyItem();
			BeanUtils.copyProperties(wxCompanyItem, wxCompany);
			//自定义属性设置填充
		}
		
		return wxCompanyItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxCompanyItem> createItems(List<WxCompany> wxCompanys) {
		List<WxCompanyItem> wxCompanyItems = new ArrayList<WxCompanyItem>();
		for (WxCompany wxCompany : wxCompanys) {
			wxCompanyItems.add(createItem(wxCompany));
		}
		return wxCompanyItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxCompany toModel() {
		WxCompany model = new WxCompany();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
