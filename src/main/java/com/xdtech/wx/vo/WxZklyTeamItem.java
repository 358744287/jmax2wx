package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import javassist.expr.NewArray;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WxZklyTeam;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see
 */
public class WxZklyTeamItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="小组名称",width=100)
	private String teamName;
	@GridColumn(title="小组对外口号",width=100)
	private String teamKouHao;
	@GridColumn(title="头像",width=100,show=false)
	private String imgUrl;
	@GridColumn(title="总销量",width=100)
	private Integer totalCount;

	List<WxZklyMemberItem> memberItems = new ArrayList<WxZklyMemberItem>();
	
	List<WxZklyDayItem> dayItems = new ArrayList<WxZklyDayItem>();
	
	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setTeamName(String teamName) {
		this.teamName = teamName;
		addQuerys("teamName", teamName);
	}
	public String getTeamName() {
		return teamName;
	}
	
	public void setTeamKouHao(String teamKouHao) {
		this.teamKouHao = teamKouHao;
		addQuerys("teamKouHao", teamKouHao);
	}
	public String getTeamKouHao() {
		return teamKouHao;
	}
	
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
		addQuerys("imgUrl", imgUrl);
	}
	public String getImgUrl() {
		return imgUrl;
	}
	
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public List<WxZklyMemberItem> getMemberItems() {
		return memberItems;
	}
	public void setMemberItems(List<WxZklyMemberItem> memberItems) {
		this.memberItems = memberItems;
	}
	public List<WxZklyDayItem> getDayItems() {
		return dayItems;
	}
	public void setDayItems(List<WxZklyDayItem> dayItems) {
		this.dayItems = dayItems;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WxZklyTeamItem createItem(WxZklyTeam wxZklyTeam) {
		WxZklyTeamItem wxZklyTeamItem = null;
		if(wxZklyTeam!=null) {
			wxZklyTeamItem = new WxZklyTeamItem();
			BeanUtils.copyProperties(wxZklyTeamItem, wxZklyTeam);
			//自定义属性设置填充
		}
		
		return wxZklyTeamItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WxZklyTeamItem> createItems(List<WxZklyTeam> wxZklyTeams) {
		List<WxZklyTeamItem> wxZklyTeamItems = new ArrayList<WxZklyTeamItem>();
		for (WxZklyTeam wxZklyTeam : wxZklyTeams) {
			wxZklyTeamItems.add(createItem(wxZklyTeam));
		}
		return wxZklyTeamItems;
	}
	
	/**
	 * 转model
	 * @return
	 */
	public WxZklyTeam toModel() {
		WxZklyTeam model = new WxZklyTeam();
		BeanUtils.copyProperties(model, this);
		return model;
	}
}
