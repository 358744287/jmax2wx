package com.xdtech.wx.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.wx.model.WeixinResponse;

import com.xdtech.web.freemark.item.GridColumn;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see
 */
public class WeixinResponseItem extends BaseItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@GridColumn(title="关键字",width=100)
	private String keyWord;
	@GridColumn(title="回复内容",width=100)
	private String resContent;
	@GridColumn(title="回复消息类型",width=100)
	private String msgType;
//	@GridColumn(title="微信号ID",width=100)
	private String accountId;

	public void setId(Long id) {
		this.id = id;
		addQuerys("id", id);
	}
	public Long getId() {
		return id;
	}
	
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
		addQuerys("keyWord", keyWord);
	}
	public String getKeyWord() {
		return keyWord;
	}
	
	public void setResContent(String resContent) {
		this.resContent = resContent;
		addQuerys("resContent", resContent);
	}
	public String getResContent() {
		return resContent;
	}
	
	public void setMsgType(String msgType) {
		this.msgType = msgType;
		addQuerys("msgType", msgType);
	}
	public String getMsgType() {
		return msgType;
	}
	
	
	
	public String getAccountId()
	{
		return accountId;
	}
	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}
	/**
	 * 根据model构建vo
	 * 
	 * @author max
	 * @return
	 */
	public static WeixinResponseItem createItem(WeixinResponse weixinResponse) {
		WeixinResponseItem weixinResponseItem = null;
		if(weixinResponse!=null) {
			weixinResponseItem = new WeixinResponseItem();
			BeanUtils.copyProperties(weixinResponseItem, weixinResponse);
			//自定义属性设置填充
		}
		
		return weixinResponseItem;
	}
	/**
	 * 根据model集合创建vo集合
	 * 
	 * @author max
	 * @return
	 */
	public static List<WeixinResponseItem> createItems(List<WeixinResponse> weixinResponses) {
		List<WeixinResponseItem> weixinResponseItems = new ArrayList<WeixinResponseItem>();
		for (WeixinResponse weixinResponse : weixinResponses) {
			weixinResponseItems.add(createItem(weixinResponse));
		}
		return weixinResponseItems;
	}
}
