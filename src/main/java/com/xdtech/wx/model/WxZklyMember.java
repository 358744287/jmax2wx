package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:24
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_ZKLY_MEMBER")
public class WxZklyMember extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_ZKLY_MEMBER")
	@GenericGenerator(name = "SEQ_WX_ZKLY_MEMBER", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_ZKLY_MEMBER") })
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="MAJOR")
	private String major;
	
	@Column(name="IMG_URL")
	private String imgUrl;
	
	public WxZklyMember() {
		super();
	}

	//多对一
	@ManyToOne
    @JoinColumn(name = "WX_ZKLY_TEAM_ID")
    WxZklyTeam wxZklyTeam;

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getMajor() {
		return major;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}

	public WxZklyTeam getWxZklyTeam()
	{
		return wxZklyTeam;
	}
	public void setWxZklyTeam(WxZklyTeam wxZklyTeam)
	{
		this.wxZklyTeam = wxZklyTeam;
	}
}
