package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_ZKLY_DAY")
public class WxZklyDay extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_ZKLY_DAY")
	@GenericGenerator(name = "SEQ_WX_ZKLY_DAY", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_ZKLY_DAY") })
	private Long id;
	
	@Column(name="DAY_TIME")
	private String dayTime;
	
	@Column(name="DAY_COUNT")
	private Integer dayCount;
	
	public WxZklyDay() {
		super();
	}

	//多对一
	@ManyToOne
    @JoinColumn(name = "WX_ZKLY_TEAM_ID")
    WxZklyTeam wxZklyTeam;

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setDayTime(String dayTime) {
		this.dayTime = dayTime;
	}
	public String getDayTime() {
		return dayTime;
	}
	public void setDayCount(Integer dayCount) {
		this.dayCount = dayCount;
	}
	public Integer getDayCount() {
		return dayCount;
	}

	public WxZklyTeam getWxZklyTeam()
	{
		return wxZklyTeam;
	}
	public void setWxZklyTeam(WxZklyTeam wxZklyTeam)
	{
		this.wxZklyTeam = wxZklyTeam;
	}
}
