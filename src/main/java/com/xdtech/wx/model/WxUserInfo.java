package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:24
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_USER_INFO")
public class WxUserInfo extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_USER_INFO")
	@GenericGenerator(name = "SEQ_WX_USER_INFO", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_USER_INFO") })
	private Long id;
	
	@Column(name="OPEN_ID")
	private String openId;
	
	@Column(name="NICK_NAME")
	private String nickName;
	
	@Column(name="SEX")
	private String sex;
	
	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="PROVINCE")
	private String province;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="HEAD_IMG_URL")
	private String headImgUrl;
	
	@Column(name="COMPANY")
	private String company;
	
	@Column(name="POSITION")
	private String position;
	@Column(name="POSITION2")
	private String position2;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="TEL")
	private String tel;
	@Column(name="SIGN_STATUS")
	private String signStatus;
	@Column(name="HCZW")
	private String hczw;
	@Column(name="YHZW")
	private String yhzw;
	
	public WxUserInfo() {
		super();
	}

	//多对一
	@ManyToOne
    @JoinColumn(name = "WX_COMPANY_ID")
    WxCompany wxCompany;

	public String getHczw() {
		return hczw;
	}
	public void setHczw(String hczw) {
		this.hczw = hczw;
	}
	public String getYhzw() {
		return yhzw;
	}
	public void setYhzw(String yhzw) {
		this.yhzw = yhzw;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getOpenId() {
		return openId;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getSex() {
		return sex;
	}
	public String getPosition2() {
		return position2;
	}
	public void setPosition2(String position2) {
		this.position2 = position2;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getProvince() {
		return province;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return city;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCompany() {
		return company;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition() {
		return position;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getTel() {
		return tel;
	}

	public WxCompany getWxCompany()
	{
		return wxCompany;
	}
	public void setWxCompany(WxCompany wxCompany)
	{
		this.wxCompany = wxCompany;
	}
	public String getSignStatus() {
		return signStatus;
	}
	public void setSignStatus(String signStatus) {
		this.signStatus = signStatus;
	}
	
}
