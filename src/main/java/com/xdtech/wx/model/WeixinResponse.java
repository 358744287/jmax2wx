package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_RESPONSE")
public class WeixinResponse extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="KEY_WORD")
	private String keyWord;
	@Column(name="RES_CONTENT")
	private String resContent;
	@Column(name="MSG_TYPE")
	private String msgType;
	@Column(name="ACCOUNT_ID")
	private String accountId;


	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setResContent(String resContent) {
		this.resContent = resContent;
	}
	public String getResContent() {
		return resContent;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgType() {
		return msgType;
	}
	public String getAccountId()
	{
		return accountId;
	}
	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}
	
}
