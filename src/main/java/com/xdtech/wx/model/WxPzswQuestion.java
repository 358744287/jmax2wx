package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_PZSW_QUESTION")
public class WxPzswQuestion extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="DEPT")
	private String dept;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="MODULE_TYPE")
	private String moduleType;
	
	@Column(name="QUESTION_NAME")
	private String questionName;
	
	@Column(name="QUESTION_REASON1")
	private String questionReason1;
	
	@Column(name="QUESTION_REASON2")
	private String questionReason2;
	
	@Column(name="QUESTION_REASON3")
	private String questionReason3;
	@Column(name="UPLOAD_IMG")
	private String uploadImg;
	
	public WxPzswQuestion() {
		super();
	}

	//一对多
	@OneToMany(fetch=FetchType.LAZY,mappedBy="wxPzswQuestion")
	List<WxPzswSolve> wxPzswSolves = new ArrayList<WxPzswSolve>();

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDept() {
		return dept;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionReason1(String questionReason1) {
		this.questionReason1 = questionReason1;
	}
	public String getQuestionReason1() {
		return questionReason1;
	}
	public void setQuestionReason2(String questionReason2) {
		this.questionReason2 = questionReason2;
	}
	public String getQuestionReason2() {
		return questionReason2;
	}
	public void setQuestionReason3(String questionReason3) {
		this.questionReason3 = questionReason3;
	}
	public String getQuestionReason3() {
		return questionReason3;
	}

	public List<WxPzswSolve> getWxPzswSolves()
	{
		return wxPzswSolves;
	}

	public void setWxPzswSolves(List<WxPzswSolve> wxPzswSolves)
	{
		this.wxPzswSolves = wxPzswSolves;
	}
	public String getUploadImg() {
		return uploadImg;
	}
	public void setUploadImg(String uploadImg) {
		this.uploadImg = uploadImg;
	}
}
