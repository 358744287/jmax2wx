package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_ZKLY_INFO")
public class WxZklyInfo extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(name="SPEAK")
	private String speak;
	
	@Column(name="SCHOOL")
	private String school;
	
	@Column(name="GRADE_LEVEL")
	private String gradeLevel;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_ZKLY_INFO")
	@GenericGenerator(name = "SEQ_WX_ZKLY_INFO", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_ZKLY_INFO") })
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="MAJOR")
	private String major;
	
	@Column(name="DORM")
	private String dorm;
	
	@Column(name="UUID")
	private String uuid;
	
	@Column(name="IMG_URL")
	private String imgUrl;
	
	@Column(name="TELPHONE")
	private String telphone;
	
	@Column(name="IDENTITY_NO")
	private String identityNo;
	
	public WxZklyInfo() {
		super();
	}


	public void setSpeak(String speak) {
		this.speak = speak;
	}
	public String getSpeak() {
		return speak;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getSchool() {
		return school;
	}
	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}
	public String getGradeLevel() {
		return gradeLevel;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getMajor() {
		return major;
	}
	public void setDorm(String dorm) {
		this.dorm = dorm;
	}
	public String getDorm() {
		return dorm;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getIdentityNo() {
		return identityNo;
	}

}
