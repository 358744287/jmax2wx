package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_RECEIVE_MSG")
public class WeixinReceiveMsg extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="TO_USER_NAME")
	private String toUserName;
	@Column(name="FROM_USER_NAME")
	private String fromUserName;
	@Column(name="MSG_TYPE")
	private String msgType;
	@Column(name="MSG_ID")
	private String msgId;
	@Column(name="CONTENT")
	private String content;
	@Column(name="RESPONSE")
	private String response;
	@Column(name="NICK_NAME")
	private String nickName;
	@Column(name="ACCOUNT_ID")
	private String accountId;


	public WeixinReceiveMsg()
	{
		super();
	}
	/**
	 * TODO 构造函数说明，请确保和下面的block tags之间保留一行空行
	 * 
	 * @param fromUserName2
	 * @param toUserName2
	 * @param createTime
	 * @param msgType2
	 * @param content2
	 * @param msgId2
	 */
	public WeixinReceiveMsg(String fromUserName, String toUserName, String createTime, String msgType, String content, String msgId)
	{
		this.fromUserName = fromUserName;
		this.toUserName = toUserName;
		this.msgType = msgType;
		this.content = content;
		this.msgId = msgId;
		
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getToUserName() {
		return toUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContent() {
		return content;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getResponse() {
		return response;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getNickName() {
		return nickName;
	}
	public String getAccountId()
	{
		return accountId;
	}
	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}
	

}
