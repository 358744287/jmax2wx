package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_ZKLY_TEAM")
public class WxZklyTeam extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_ZKLY_TEAM")
	@GenericGenerator(name = "SEQ_WX_ZKLY_TEAM", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_ZKLY_TEAM") })
	private Long id;
	
	@Column(name="TEAM_NAME")
	private String teamName;
	
	@Column(name="TEAM_KOU_HAO")
	private String teamKouHao;
	
	@Column(name="IMG_URL")
	private String imgUrl;
	
	public WxZklyTeam() {
		super();
	}

	//一对多
	@OneToMany(fetch=FetchType.LAZY,mappedBy="wxZklyTeam")
	List<WxZklyMember> wxZklyMembers = new ArrayList<WxZklyMember>();
	//一对多
	@OneToMany(fetch=FetchType.LAZY,mappedBy="wxZklyTeam")
	List<WxZklyDay> wxZklyDays = new ArrayList<WxZklyDay>();

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamKouHao(String teamKouHao) {
		this.teamKouHao = teamKouHao;
	}
	public String getTeamKouHao() {
		return teamKouHao;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}

	public List<WxZklyMember> getWxZklyMembers()
	{
		return wxZklyMembers;
	}

	public void setWxZklyMembers(List<WxZklyMember> wxZklyMembers)
	{
		this.wxZklyMembers = wxZklyMembers;
	}
	public List<WxZklyDay> getWxZklyDays()
	{
		return wxZklyDays;
	}

	public void setWxZklyDays(List<WxZklyDay> wxZklyDays)
	{
		this.wxZklyDays = wxZklyDays;
	}
}
