package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_PZSW_SOLVE")
public class WxPzswSolve extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="CHOOSE")
	private String choose;
	
	public WxPzswSolve() {
		super();
	}

	//多对一
	@ManyToOne
    @JoinColumn(name = "WX_PZSW_QUESTION_ID")
    WxPzswQuestion wxPzswQuestion;

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	

	public String getChoose() {
		return choose;
	}
	public void setChoose(String choose) {
		this.choose = choose;
	}
	public WxPzswQuestion getWxPzswQuestion()
	{
		return wxPzswQuestion;
	}
	public void setWxPzswQuestion(WxPzswQuestion wxPzswQuestion)
	{
		this.wxPzswQuestion = wxPzswQuestion;
	}
}
