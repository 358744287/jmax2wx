package com.xdtech.wx.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2016-03-10 22:36:32
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_ACCOUNT")
public class WeixinAccount extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="NAME")
	private String name;
	@Column(name="TOKEN")
	private String token;
	@Column(name="NUM")
	private String num;
	//微信对应原始id
	@Column(name="ORIGINAL_ID")
	private String originalId;
	@Column(name="ACCOUNT_TYPE")
	private String accountType;
	@Column(name="EMAIL")
	private String email;
	@Column(name="APP_ID")
	private String appId;
	@Column(name="ACCOUNT_DESC")
	private String accountDesc;
	@Column(name="APP_SECRET")
	private String appSecret;
	@Column(name="ACCESS_TOKEN")
	private String accessToken;
	@Column(name="SET_TOKEN_TIME")
	private String setTokenTime;
	@Column(name="USER_NAME")
	private String userName;
	
	@Column(name="WELCOME_TEXT")
	@Lob
	private String welcomeText;
	
	public WeixinAccount() {
		super();
	}
	
	
	public WeixinAccount(String name, String token,String num, String originalId,
			String accountType, String appId, String accountDesc,
			String appSecret, String userName,String email, String welcomeText) {
		super();
		this.name = name;
		this.token = token;
		this.num = num;
		this.originalId = originalId;
		this.accountType = accountType;
		this.appId = appId;
		this.accountDesc = accountDesc;
		this.appSecret = appSecret;
		this.userName = userName;
		this.email = email;
		this.welcomeText = welcomeText;
	}


	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}
	public String getOriginalId() {
		return originalId;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAccountDesc(String accountDesc) {
		this.accountDesc = accountDesc;
	}
	public String getAccountDesc() {
		return accountDesc;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setSetTokenTime(String setTokenTime) {
		this.setTokenTime = setTokenTime;
	}
	public String getSetTokenTime() {
		return setTokenTime;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	public String getWelcomeText()
	{
		return welcomeText;
	}
	public void setWelcomeText(String welcomeText)
	{
		this.welcomeText = welcomeText;
	}

}
