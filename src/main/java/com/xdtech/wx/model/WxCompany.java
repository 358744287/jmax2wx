package com.xdtech.wx.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import com.xdtech.core.model.BaseModel;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see 
 */
@Entity
@Table(name="WX_COMPANY")
public class WxCompany extends BaseModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="SEQ_WX_COMPANY")
	@GenericGenerator(name = "SEQ_WX_COMPANY", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequence_name", value = "SEQ_WX_COMPANY") })
	private Long id;
	
	@Column(name="CODE")
	private String code;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="PERSON_COUNT")
	private Integer personCount;
	
	public WxCompany() {
		super();
	}

	//一对多
	@OneToMany(fetch=FetchType.LAZY,mappedBy="wxCompany")
	List<WxUserInfo> wxUserInfos = new ArrayList<WxUserInfo>();

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setPersonCount(Integer personCount) {
		this.personCount = personCount;
	}
	public Integer getPersonCount() {
		return personCount;
	}

	public List<WxUserInfo> getWxUserInfos()
	{
		return wxUserInfos;
	}

	public void setWxUserInfos(List<WxUserInfo> wxUserInfos)
	{
		this.wxUserInfos = wxUserInfos;
	}
}
