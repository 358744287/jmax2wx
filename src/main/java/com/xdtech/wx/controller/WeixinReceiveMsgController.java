package com.xdtech.wx.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WeixinReceiveMsgService;
import com.xdtech.wx.vo.WeixinReceiveMsgItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:00:10
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WeixinReceiveMsgController extends BaseController{
	private Log log = LogFactory.getLog(WeixinReceiveMsgController.class);
	@Autowired
	private WeixinReceiveMsgService weixinReceiveMsgService;
	@RequestMapping(value="/weixinReceiveMsg.do",params = "weixinReceiveMsg")
	public ModelAndView skipWeixinReceiveMsg() {
		return new ModelAndView("wx/weixinReceiveMsg/weixinReceiveMsg_ftl");
	}
	
	@RequestMapping(value="/weixinReceiveMsg.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WeixinReceiveMsgItem item,Pagination pg) {
		Map<String, Object> results =  weixinReceiveMsgService.loadPageDataByConditions(pg,item,"findWeixinReceiveMsgByCondition");
		return results;
	}
	
	@RequestMapping(value="/weixinReceiveMsg.do",params = "editWeixinReceiveMsg")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWeixinReceiveMsg(HttpServletRequest request,Long weixinReceiveMsgId) {
		if (weixinReceiveMsgId!=null) {
			request.setAttribute("weixinReceiveMsgItem", weixinReceiveMsgService.loadWeixinReceiveMsgItem(weixinReceiveMsgId));
		}
		return new ModelAndView("wx/weixinReceiveMsg/editWeixinReceiveMsg_ftl");
	}
	
	@RequestMapping(value="/weixinReceiveMsg.do",params = "saveWeixinReceiveMsg")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWeixinReceiveMsg(WeixinReceiveMsgItem item) {
		ResultMessage r = new ResultMessage();
		if (weixinReceiveMsgService.saveOrUpdateWeixinReceiveMsg(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/weixinReceiveMsg.do",params = "deleteWeixinReceiveMsgItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWeixinReceiveMsgItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> weixinReceiveMsgIds = new ArrayList<Long>();
			for (String id : tempIds) {
				weixinReceiveMsgIds.add(Long.valueOf(id));
			}
			weixinReceiveMsgService.deleteWeixinReceiveMsgInfo(weixinReceiveMsgIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
