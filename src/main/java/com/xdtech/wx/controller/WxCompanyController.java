package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxCompanyService;
import com.xdtech.wx.vo.WxCompanyItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2018-01-04 11:30:21
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxCompanyController extends BaseController{
	@Autowired
	private WxCompanyService wxCompanyService;
	@RequestMapping(value="/wxCompany.do",params = "wxCompany")
	public ModelAndView skipWxCompany() {
		return new ModelAndView("wx/wxCompany/wxCompany_ftl");
	}
	
	@RequestMapping(value="/wxCompany.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxCompanyItem item,Pagination pg) {
		Map<String, Object> results =  wxCompanyService.loadPageDataByConditions(pg,item,"findWxCompanyByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxCompany.do",params = "editWxCompany")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxCompany(HttpServletRequest request,Long wxCompanyId) {
		if (wxCompanyId!=null) {
			request.setAttribute("wxCompanyItem", wxCompanyService.loadWxCompanyItem(wxCompanyId));
		}
		return new ModelAndView("wx/wxCompany/editWxCompany_ftl");
	}
	
	@RequestMapping(value="/wxCompany.do",params = "saveWxCompany")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxCompany(WxCompanyItem item) {
		ResultMessage r = new ResultMessage();
		if (wxCompanyService.saveOrUpdateWxCompany(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/wxCompany.do",params = "deleteWxCompanyItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxCompanyItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxCompanyIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxCompanyIds.add(Long.valueOf(id));
			}
			wxCompanyService.deleteWxCompanyInfo(wxCompanyIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
