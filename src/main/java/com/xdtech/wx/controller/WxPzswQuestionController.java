package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxPzswQuestionService;
import com.xdtech.wx.vo.WxPzswQuestionItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:02
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxPzswQuestionController extends BaseController{
	@Autowired
	private WxPzswQuestionService wxPzswQuestionService;
	@RequestMapping(value="/wxPzswQuestion.do",params = "wxPzswQuestion")
	public ModelAndView skipWxPzswQuestion() {
		return new ModelAndView("wx/wxPzswQuestion/wxPzswQuestion_ftl");
	}
	
	@RequestMapping(value="/wxPzswQuestion.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxPzswQuestionItem item,Pagination pg) {
		Map<String, Object> results =  wxPzswQuestionService.loadPageDataByConditions(pg,item,"findWxPzswQuestionByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxPzswQuestion.do",params = "editWxPzswQuestion")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxPzswQuestion(HttpServletRequest request,Long wxPzswQuestionId) {
		if (wxPzswQuestionId!=null) {
			request.setAttribute("wxPzswQuestionItem", wxPzswQuestionService.loadWxPzswQuestionItem(wxPzswQuestionId));
		}
		return new ModelAndView("wx/wxPzswQuestion/editWxPzswQuestion_ftl");
	}
	
	@RequestMapping(value="/wxPzswQuestion.do",params = "saveWxPzswQuestion")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxPzswQuestion(WxPzswQuestionItem item) {
		ResultMessage r = new ResultMessage();
		if (wxPzswQuestionService.saveOrUpdateWxPzswQuestion(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/wxPzswQuestion.do",params = "deleteWxPzswQuestionItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxPzswQuestionItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxPzswQuestionIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxPzswQuestionIds.add(Long.valueOf(id));
			}
			wxPzswQuestionService.deleteWxPzswQuestionInfo(wxPzswQuestionIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
