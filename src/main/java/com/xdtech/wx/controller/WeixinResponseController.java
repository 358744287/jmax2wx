package com.xdtech.wx.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WeixinResponseService;
import com.xdtech.wx.vo.WeixinResponseItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2016-03-11 15:16:55
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WeixinResponseController extends BaseController{
	private Log log = LogFactory.getLog(WeixinResponseController.class);
	@Autowired
	private WeixinResponseService weixinResponseService;
	@RequestMapping(value="/weixinResponse.do",params = "weixinResponse")
	public ModelAndView skipWeixinResponse() {
		return new ModelAndView("wx/weixinResponse/weixinResponse_ftl");
	}
	
	@RequestMapping(value="/weixinResponse.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WeixinResponseItem item,Pagination pg) {
		Map<String, Object> results =  weixinResponseService.loadPageDataByConditions(pg,item,"findWeixinResponseByCondition");
		return results;
	}
	
	@RequestMapping(value="/weixinResponse.do",params = "editWeixinResponse")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWeixinResponse(HttpServletRequest request,Long weixinResponseId) {
		if (weixinResponseId!=null) {
			request.setAttribute("weixinResponseItem", weixinResponseService.loadWeixinResponseItem(weixinResponseId));
		}
		return new ModelAndView("wx/weixinResponse/editWeixinResponse_ftl");
	}
	
	@RequestMapping(value="/weixinResponse.do",params = "saveWeixinResponse")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWeixinResponse(WeixinResponseItem item) {
		ResultMessage r = new ResultMessage();
		if (weixinResponseService.saveOrUpdateWeixinResponse(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/weixinResponse.do",params = "deleteWeixinResponseItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWeixinResponseItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> weixinResponseIds = new ArrayList<Long>();
			for (String id : tempIds) {
				weixinResponseIds.add(Long.valueOf(id));
			}
			weixinResponseService.deleteWeixinResponseInfo(weixinResponseIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
