package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javassist.expr.NewArray;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.common.utils.ApplicationContextUtil;
import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxZklyMemberService;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyMemberItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:24
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxZklyMemberController extends BaseController{
	@Autowired
	private WxZklyMemberService wxZklyMemberService;
	@Autowired
	private WxZklyTeamService wxZklyTeamService;
	@RequestMapping(value="/wxZklyMember.do",params = "wxZklyMember")
	public ModelAndView skipWxZklyMember() {
		return new ModelAndView("wx/wxZklyMember/wxZklyMember_ftl");
	}
	
	@RequestMapping(value="/wxZklyMember.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxZklyMemberItem item,Pagination pg) {
		Map<String, Object> results =  wxZklyMemberService.loadPageDataByConditions(pg,item,"findWxZklyMemberByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxZklyMember.do",params = "editWxZklyMember")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxZklyMember(HttpServletRequest request,Long wxZklyMemberId,Long wxZklyTeamId) {
		if (wxZklyMemberId!=null) {
			request.setAttribute("wxZklyMemberItem", wxZklyMemberService.loadWxZklyMemberItem(wxZklyMemberId));
		}
		else 
		{
			request.setAttribute("wxZklyMemberItem", new WxZklyMemberItem(wxZklyTeamId));
		}
		
		return new ModelAndView("wx/wxZklyMember/editWxZklyMember_ftl");
	}
	
	@RequestMapping(value="/wxZklyMember.do",params = "saveWxZklyMember")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxZklyMember(WxZklyMemberItem item) {
		ResultMessage r = new ResultMessage();
		if (wxZklyMemberService.saveOrUpdateWxZklyMember(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		ApplicationContextUtil.getApplication().setAttribute("teamMapList", wxZklyTeamService.loadTongJi());

		return r;
	}
	
	@RequestMapping(value="/wxZklyMember.do",params = "deleteWxZklyMemberItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxZklyMemberItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxZklyMemberIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxZklyMemberIds.add(Long.valueOf(id));
			}
			wxZklyMemberService.deleteWxZklyMemberInfo(wxZklyMemberIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
