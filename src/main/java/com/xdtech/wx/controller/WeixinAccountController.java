package com.xdtech.wx.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WeixinAccountService;
import com.xdtech.wx.vo.WeixinAccountItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2016-03-10 22:36:32
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WeixinAccountController extends BaseController{
	@Autowired
	private WeixinAccountService weixinAccountService;
	@RequestMapping(value="/weixinAccount.do",params = "weixinAccount")
	public ModelAndView skipWeixinAccount() {
		return new ModelAndView("wx/weixinAccount/weixinAccount_ftl");
	}
	
	@RequestMapping(value="/weixinAccount.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WeixinAccountItem item,Pagination pg) {
		Map<String, Object> results =  weixinAccountService.loadPageDataByConditions(pg,item,"findWeixinAccountByCondition");
		return results;
	}
	
	@RequestMapping(value="/weixinAccount.do",params = "editWeixinAccount")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWeixinAccount(HttpServletRequest request,Long weixinAccountId) {
		if (weixinAccountId!=null) {
			request.setAttribute("weixinAccountItem", weixinAccountService.loadWeixinAccountItem(weixinAccountId));
		}
		return new ModelAndView("wx/weixinAccount/editWeixinAccount_ftl");
	}
	
	@RequestMapping(value="/weixinAccount.do",params = "saveWeixinAccount")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWeixinAccount(WeixinAccountItem item) {
		ResultMessage r = new ResultMessage();
		if (weixinAccountService.saveOrUpdateWeixinAccount(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/weixinAccount.do",params = "deleteWeixinAccountItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWeixinAccountItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> weixinAccountIds = new ArrayList<Long>();
			for (String id : tempIds) {
				weixinAccountIds.add(Long.valueOf(id));
			}
			weixinAccountService.deleteWeixinAccountInfo(weixinAccountIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
