package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.common.utils.ApplicationContextUtil;
import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyTeamItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:20
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxZklyTeamController extends BaseController{
	@Autowired
	private WxZklyTeamService wxZklyTeamService;
	@RequestMapping(value="/wxZklyTeam.do",params = "wxZklyTeam")
	public ModelAndView skipWxZklyTeam() {
		return new ModelAndView("wx/wxZklyTeam/wxZklyTeam_ftl");
	}
	
	@RequestMapping(value="/wxZklyTeam.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxZklyTeamItem item,Pagination pg) {
		Map<String, Object> results =  wxZklyTeamService.loadPageDataByConditions(pg,item,"findWxZklyTeamByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxZklyTeam.do",params = "editWxZklyTeam")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxZklyTeam(HttpServletRequest request,Long wxZklyTeamId) {
		if (wxZklyTeamId!=null) {
			request.setAttribute("wxZklyTeamItem", wxZklyTeamService.loadWxZklyTeamItem(wxZklyTeamId));
		}
		return new ModelAndView("wx/wxZklyTeam/editWxZklyTeam_ftl");
	}
	
	@RequestMapping(value="/wxZklyTeam.do",params = "saveWxZklyTeam")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxZklyTeam(WxZklyTeamItem item) {
		ResultMessage r = new ResultMessage();
		if (wxZklyTeamService.saveOrUpdateWxZklyTeam(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		Map<String, Object> mapInfo = (Map<String, Object>) ApplicationContextUtil.getApplication().getAttribute("teamMapList");
		ApplicationContextUtil.getApplication().setAttribute("teamMapList", wxZklyTeamService.loadTongJi());
		
		return r;
	}
	
	@RequestMapping(value="/wxZklyTeam.do",params = "deleteWxZklyTeamItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxZklyTeamItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxZklyTeamIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxZklyTeamIds.add(Long.valueOf(id));
			}
			wxZklyTeamService.deleteWxZklyTeamInfo(wxZklyTeamIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
