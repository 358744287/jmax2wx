package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.common.utils.ApplicationContextUtil;
import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxZklyDayService;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.vo.WxZklyDayItem;
import com.xdtech.wx.vo.WxZklyMemberItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-09-22 00:26:27
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxZklyDayController extends BaseController{
	@Autowired
	private WxZklyDayService wxZklyDayService;
	@Autowired
	private WxZklyTeamService wxZklyTeamService;
	@RequestMapping(value="/wxZklyDay.do",params = "wxZklyDay")
	public ModelAndView skipWxZklyDay() {
		return new ModelAndView("wx/wxZklyDay/wxZklyDay_ftl");
	}
	
	@RequestMapping(value="/wxZklyDay.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxZklyDayItem item,Pagination pg) {
		Map<String, Object> results =  wxZklyDayService.loadPageDataByConditions(pg,item,"findWxZklyDayByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxZklyDay.do",params = "editWxZklyDay")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxZklyDay(HttpServletRequest request,Long wxZklyDayId,Long wxZklyTeamId) {
		if (wxZklyDayId!=null) {
			request.setAttribute("wxZklyDayItem", wxZklyDayService.loadWxZklyDayItem(wxZklyDayId));
		}
		else 
		{
			request.setAttribute("wxZklyDayItem", new WxZklyDayItem(wxZklyTeamId));
		}
		request.setAttribute("wxZklyTeamId", wxZklyTeamId);
		return new ModelAndView("wx/wxZklyDay/editWxZklyDay_ftl");
	}
	
	@RequestMapping(value="/wxZklyDay.do",params = "saveWxZklyDay")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxZklyDay(WxZklyDayItem item) {
		ResultMessage r = new ResultMessage();
		if (wxZklyDayService.saveOrUpdateWxZklyDay(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		ApplicationContextUtil.getApplication().setAttribute("teamMapList", wxZklyTeamService.loadTongJi());
		return r;
	}
	
	@RequestMapping(value="/wxZklyDay.do",params = "deleteWxZklyDayItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxZklyDayItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxZklyDayIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxZklyDayIds.add(Long.valueOf(id));
			}
			wxZklyDayService.deleteWxZklyDayInfo(wxZklyDayIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
