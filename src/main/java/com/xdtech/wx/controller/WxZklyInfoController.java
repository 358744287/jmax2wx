package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxZklyInfoService;
import com.xdtech.wx.vo.WxZklyInfoItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-09-14 23:28:40
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxZklyInfoController extends BaseController{
	@Autowired
	private WxZklyInfoService wxZklyInfoService;
	@RequestMapping(value="/wxZklyInfo.do",params = "wxZklyInfo")
	public ModelAndView skipWxZklyInfo() {
		return new ModelAndView("wx/wxZklyInfo/wxZklyInfo_ftl");
	}
	
	@RequestMapping(value="/wxZklyInfo.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxZklyInfoItem item,Pagination pg) {
		Map<String, Object> results =  wxZklyInfoService.loadPageDataByConditions(pg,item,"findWxZklyInfoByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxZklyInfo.do",params = "editWxZklyInfo")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxZklyInfo(HttpServletRequest request,Long wxZklyInfoId) {
		if (wxZklyInfoId!=null) {
			request.setAttribute("wxZklyInfoItem", wxZklyInfoService.loadWxZklyInfoItem(wxZklyInfoId));
		}
		return new ModelAndView("wx/wxZklyInfo/editWxZklyInfo_ftl");
	}
	
	@RequestMapping(value="/wxZklyInfo.do",params = "saveWxZklyInfo")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxZklyInfo(WxZklyInfoItem item) {
		ResultMessage r = new ResultMessage();
		if (wxZklyInfoService.saveOrUpdateWxZklyInfo(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/wxZklyInfo.do",params = "deleteWxZklyInfoItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxZklyInfoItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxZklyInfoIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxZklyInfoIds.add(Long.valueOf(id));
			}
			wxZklyInfoService.deleteWxZklyInfoInfo(wxZklyInfoIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
