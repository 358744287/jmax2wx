package com.xdtech.wx.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xdtech.sys.aspect.SystemControllerLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xdtech.wx.service.WxPzswSolveService;
import com.xdtech.wx.vo.WxPzswSolveItem;
import com.xdtech.web.controller.BaseController;
import com.xdtech.web.model.Pagination;
import com.xdtech.web.model.ResultMessage;

/**
 * 
 * @author max.zheng
 * @create 2017-05-08 14:17:05
 * @since 1.0
 * @see
 */
@Controller
@Scope("prototype")
public class WxPzswSolveController extends BaseController{
	@Autowired
	private WxPzswSolveService wxPzswSolveService;
	@RequestMapping(value="/wxPzswSolve.do",params = "wxPzswSolve")
	public ModelAndView skipWxPzswSolve() {
		return new ModelAndView("wx/wxPzswSolve/wxPzswSolve_ftl");
	}
	
	@RequestMapping(value="/wxPzswSolve.do",params="loadList")
	@ResponseBody
	public Map<String, Object> loadList(WxPzswSolveItem item,Pagination pg) {
		Map<String, Object> results =  wxPzswSolveService.loadPageDataByConditions(pg,item,"findWxPzswSolveByCondition");
		return results;
	}
	
	@RequestMapping(value="/wxPzswSolve.do",params = "editWxPzswSolve")
	@SystemControllerLog(description = "编辑")
	public ModelAndView editWxPzswSolve(HttpServletRequest request,Long wxPzswSolveId) {
		if (wxPzswSolveId!=null) {
			request.setAttribute("wxPzswSolveItem", wxPzswSolveService.loadWxPzswSolveItem(wxPzswSolveId));
		}
		return new ModelAndView("wx/wxPzswSolve/editWxPzswSolve_ftl");
	}
	
	@RequestMapping(value="/wxPzswSolve.do",params = "saveWxPzswSolve")
	@ResponseBody
	@SystemControllerLog(description = "保存")
	public ResultMessage saveWxPzswSolve(WxPzswSolveItem item) {
		ResultMessage r = new ResultMessage();
		if (wxPzswSolveService.saveOrUpdateWxPzswSolve(item)) {
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}
	
	@RequestMapping(value="/wxPzswSolve.do",params = "deleteWxPzswSolveItems")
	@ResponseBody
	@SystemControllerLog(description = "删除")
	public ResultMessage deleteWxPzswSolveItems(String ids) {
		ResultMessage r = new ResultMessage();
		if (StringUtils.isNotEmpty(ids)) {
			String[] tempIds = ids.split(",");
			List<Long> wxPzswSolveIds = new ArrayList<Long>();
			for (String id : tempIds) {
				wxPzswSolveIds.add(Long.valueOf(id));
			}
			wxPzswSolveService.deleteWxPzswSolveInfo(wxPzswSolveIds);
			r.setSuccess(true);
		}else {
			r.setSuccess(false);
		}
		return r;
	}

}
