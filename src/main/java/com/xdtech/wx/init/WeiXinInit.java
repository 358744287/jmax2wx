/*
 * Project Name: jmax
 * File Name: WeiXinInit.java
 * Copyright: Copyright(C) 1985-2016 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.wx.init;

import java.util.List;

import javax.servlet.ServletContext;

import com.xdtech.common.service.impl.BaseService;
import com.xdtech.common.utils.ApplicationContextUtil;
import com.xdtech.common.utils.EmptyUtil;
import com.xdtech.core.model.BaseModel;
import com.xdtech.sys.init.SysAbstractInit;
import com.xdtech.sys.model.CodeValue;
import com.xdtech.sys.model.DictionaryCode;
import com.xdtech.sys.model.MenuFunction;
import com.xdtech.sys.model.Params;
import com.xdtech.wx.model.WeixinAccount;
import com.xdtech.wx.service.WxZklyTeamService;
import com.xdtech.wx.util.MessageUtil;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2016-3-11 上午10:51:54
 */
public class WeiXinInit extends SysAbstractInit {
	/* (非 Javadoc)
	 * initingToDb
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param baseService
	 * @see com.xdtech.core.init.SysInitOperation#initingToDb(com.xdtech.common.service.impl.BaseService)
	 */
	@Override
	public void initingToDb(BaseService<BaseModel> baseService)
	{
		List params = (List) baseService.findByHql("from Params where code=? and codeValue=?", "WeiXinInit","true");
		log.info("微信模块数据库初始化......");
		if (EmptyUtil.isEmpty(params)) {
			MenuFunction m1 = new MenuFunction();
			m1.setIconName("icon-forum");
			m1.setNameCN("微信管理");
			m1.setOperationCode("");
			m1.setPageUrl(null);
			baseService.save(m1);
			
			MenuFunction m11 = new MenuFunction();
			m11.setIconName("icon-blue-customer");
			m11.setNameCN("公众号管理");
			m11.setOperationCode("weinxinAccount-manage");
			m11.setPageUrl("weixinAccount.do?weixinAccount");
			m11.setParent(m1);
			baseService.save(m11);
			
			MenuFunction m21 = new MenuFunction();
			m21.setIconName("icon-forum");
			m21.setNameCN("消息列表");
			m21.setOperationCode("message-manage");
			m21.setPageUrl("weixinReceiveMsg.do?weixinReceiveMsg");
			m21.setParent(m1);
			baseService.save(m21);
			
			
			MenuFunction m31 = new MenuFunction();
			m31.setIconName("icon-save");
			m31.setNameCN("自动回复");
			m31.setOperationCode("autoResponse-manage");
			m31.setPageUrl("weixinResponse.do?weixinResponse");
			m31.setParent(m1);
			baseService.save(m31);
			
			//初始化内容
			WeixinAccount weixinAccount = new WeixinAccount("max奋斗", "jmax4you", "jmax4ever", "gh_6b65363791fc", "subscription", "wxb7db1ebdb7ef8470", "JMax开发平台", "41f3db809973d1965e2d869b2c29f97a", "root", "358744287@qq.com",MessageUtil.WELCOME_TEXT);
			baseService.save(weixinAccount);
			
			DictionaryCode dictionaryCode = new DictionaryCode("WX_ACOUNT_Type","微信公众号类型");
			baseService.save(dictionaryCode);
			baseService.save(new CodeValue("service","服务号",1,dictionaryCode));
			baseService.save(new CodeValue("subscription","订阅号",2,dictionaryCode));
			
			
			Params p = new Params("WeiXinInit", "true", "微信模块数据库初始化标记");
			baseService.save(p);
			log.info("微信模块数据库初始化......结束");
		}else {
			log.info("微信模块数据库已经初始化......完毕");
		}
	}

	/* (非 Javadoc)
	 * initingToCache
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param baseService
	 * @see com.xdtech.core.init.SysInitOperation#initingToCache(com.xdtech.common.service.impl.BaseService)
	 */
	@Override
	public void initingToCache(BaseService<BaseModel> baseService)
	{
		// TODO Auto-generated method stub
		
	}

	/* (非 Javadoc)
	 * initingToWebApplication
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param baseService
	 * @see com.xdtech.core.init.SysInitOperation#initingToWebApplication(com.xdtech.common.service.impl.BaseService)
	 */
	@Override
	public void initingToWebApplication(BaseService<BaseModel> baseService)
	{
//		WeixinUtil.getWxConfig();
//		ServletContext application = ApplicationContextUtil.getApplication();
		WxZklyTeamService wxZklyTeamService = (WxZklyTeamService) ApplicationContextUtil.getContext().getBean(WxZklyTeamService.class);
		ApplicationContextUtil.getApplication().setAttribute("teamMapList", wxZklyTeamService.loadTongJi());

	}

	/* (非 Javadoc)
	 * getInitOrder
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @return
	 * @see com.xdtech.core.init.SysInitOperation#getInitOrder()
	 */
	@Override
	public int getInitOrder()
	{
		return 10;
	}

}
